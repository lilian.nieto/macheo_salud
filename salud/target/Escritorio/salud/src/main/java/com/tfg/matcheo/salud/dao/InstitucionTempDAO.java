/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.matcheo.salud.dao;

import com.tfg.matcheo.salud.entity.InstitucionTemp;

/**
 *
 * @author lilian
 */
public class InstitucionTempDAO extends GenericDAO<InstitucionTemp>{
    private static final long serialVersionUID = 1L;

    public InstitucionTempDAO() {
        super(InstitucionTemp.class);
    }
    
}
