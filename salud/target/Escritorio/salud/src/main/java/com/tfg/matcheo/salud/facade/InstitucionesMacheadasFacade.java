/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.matcheo.salud.facade;

import com.tfg.matcheo.salud.beans.AbstractMB;
import com.tfg.matcheo.salud.dao.InstitucionesMacheadasDAO;
import com.tfg.matcheo.salud.entity.InstitucionesMacheadas;
import com.tfg.matcheo.salud.entity.util.JpaUtil;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 *
 * @author lilian
 */
public class InstitucionesMacheadasFacade extends AbstractMB implements Serializable{
    
    private static final long serialVersionUID = 1L;
    private InstitucionesMacheadasDAO institucionesMacheadasDAO = new InstitucionesMacheadasDAO();  
    
    @SuppressWarnings("unchecked")
    public static <T> T findBean(String beanName) {
        FacesContext context = FacesContext.getCurrentInstance();
        return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
    }
    
    public void createInstitucionesMacheadas(InstitucionesMacheadas institucionesMacheadas) throws Exception {
        //try{
        institucionesMacheadasDAO.beginTransaction();
        institucionesMacheadasDAO.save(institucionesMacheadas);
        institucionesMacheadasDAO.commitAndCloseTransaction();
        //}catch(Exception e){
        //    institucionesMacheadasDAO.commitAndCloseTransaction();
        //    System.out.println("KORE " + e.getMessage());
        //}
        
    }

    public void updateInstitucionesMacheadas(InstitucionesMacheadas institucionesMacheadas) throws Exception {
        institucionesMacheadasDAO.beginTransaction();          
        institucionesMacheadasDAO.update(institucionesMacheadas);        
        institucionesMacheadasDAO.commitAndCloseTransaction();        
    }

    public void deleteInstitucionesMacheadas(InstitucionesMacheadas institucionesMacheadas) throws Exception {
        institucionesMacheadasDAO.beginTransaction();
        institucionesMacheadasDAO.delete(institucionesMacheadas);
        institucionesMacheadasDAO.commitAndCloseTransaction();
    }
    
    public void deleteAllInstitucionesMacheadas() throws Exception {
        institucionesMacheadasDAO.beginTransaction();
        
        EntityManagerFactory emf = JpaUtil.getEntityManagerFactory();
        EntityManager em = emf.createEntityManager();
    
        Query q;
        q = em.createQuery("DELETE FROM InstitucionesMacheadas ");
        q.executeUpdate();
           
        institucionesMacheadasDAO.commitAndCloseTransaction();
    }
}
