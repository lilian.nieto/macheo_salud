/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.matcheo.salud.model;

import com.tfg.matcheo.salud.beans.MacheoNombresInstitucionesEducativasBean;
import com.tfg.matcheo.salud.entity.InstitucionTemp;
import com.tfg.matcheo.salud.entity.util.JpaUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author lilian
 */
@ManagedBean(name = "institucionTempLazyList")
@ViewScoped
public class InstitucionTempLazyList extends LazyDataModel<InstitucionTemp> implements Serializable{
    
    
    
    private static final long serialVersionUID = 1L;    
    private List<InstitucionTemp> institucionesTemp;    
    
    private int rowCount = 0;
    
    EntityManagerFactory emf = JpaUtil.getEntityManagerFactory();
    EntityManager em = emf.createEntityManager();
        
    @SuppressWarnings("unchecked")
    public static <T> T findBean(String beanName) {
        try{
            FacesContext context = FacesContext.getCurrentInstance();
            return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
        }catch(Exception e){
            return null;
        }
    }
    
    
    // el load del Lazy es el método principal y el que se sobreescribe con cada update en la interfaz
    @Override
    public List<InstitucionTemp> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map<String, String> filters) {
     
        int archivo = 2;
        String departamento = "CONCEPCION";
        MacheoNombresInstitucionesEducativasBean bean = findBean("macheoNombresInstitucionesEducativasBean");
        if(bean != null){
            if((Integer) bean.getSelectOneMenuArchivo().getValue() != null){
                if((Integer) bean.getSelectOneMenuArchivo().getValue() == 2){
                    archivo = 1;
                }else{
                    archivo = 2;
                }
            }
            if((String) bean.getSelectOneMenuDepartamento().getValue() != null){
                departamento = (String) bean.getSelectOneMenuDepartamento().getValue();
            }
        }
        
        // <editor-fold defaultstate="collapsed" desc="Filtros">
        // definicion de filtros especificos menos filtro de fecha
        String queryFilter = null;
        String querySort = null;
        if ( !filters.isEmpty() ) {
            if (filters.containsKey("codigoPrincipal")){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.codigoPrincipal) LIKE lower('%" + filters.get("codigoPrincipal") + "%') ";
                } else{
                    queryFilter = "lower(it.codigoPrincipal) LIKE lower('%" + filters.get("codigoPrincipal") + "%') ";
                }
            }
            if (filters.containsKey("nombreInstitucionOriginal")){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.nombreInstitucionOriginal) LIKE lower('%" + filters.get("nombreInstitucionOriginal") + "%') ";
                } else{
                    queryFilter = "lower(it.nombreInstitucionOriginal) LIKE lower('%" + filters.get("nombreInstitucionOriginal") + "%') ";
                }
            }
            if (filters.containsKey("codigoSecundario")){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.codigoSecundario) LIKE lower('%" + filters.get("codigoSecundario") + "%') ";
                } else{
                    queryFilter = "lower(it.codigoSecundario) LIKE lower('%" + filters.get("codigoSecundario") + "%') ";
                }
            }
            if (filters.containsKey("nombreDistrito")){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.nombreDistrito) LIKE lower('%" + filters.get("nombreDistrito") + "%') ";
                } else{
                    queryFilter = "lower(it.nombreDistrito) LIKE lower('%" + filters.get("nombreDistrito") + "%') ";
                }
            }

        }      
        // definicion de order
        if(sortField != null) {
            querySort = "ORDER BY it." + sortField;
            querySort = querySort + (SortOrder.ASCENDING.equals(sortOrder) ? " ASC " : " DESC ");
        }
        // </editor-fold>

        String query;
        String queryCount;
        Query q;
            
        queryCount = "SELECT count(it) FROM InstitucionTemp it "
                + "WHERE it.tipo = " + archivo + " ";
        if(archivo == 2 && departamento.compareTo("PARQUE REGIONAL VIRTUAL HOSPITALES ESPECIALIZADOS")!=0){
            queryCount = queryCount + "AND it.nombreDepartamento LIKE '" + departamento + "' ";
        }
        query = "SELECT it FROM InstitucionTemp it "
                + "WHERE it.tipo = "+ archivo + " ";
        if(archivo == 2 && departamento.compareTo("PARQUE REGIONAL VIRTUAL HOSPITALES ESPECIALIZADOS")!=0){
            query = query + "AND it.nombreDepartamento LIKE '" + departamento + "' ";
        }
        
        if (queryFilter != null){
            queryCount = queryCount + "AND " + queryFilter;
            query = query + "AND " + queryFilter;
        }
        if(querySort != null){
            query = query + querySort;
        }
        
        try {
            // query de cantidad de registros
            q = em.createQuery(queryCount);
            q.setHint("javax.persistence.cache.storeMode", "REFRESH");
            rowCount = ((Long) q.getSingleResult()).intValue(); 
            setRowCount(rowCount);
            
            // query de datos a desplegar
            q = em.createQuery(query);
            q.setFirstResult(startingAt);
            q.setMaxResults(maxPerPage);
            q.setHint("javax.persistence.cache.storeMode", "REFRESH");
            institucionesTemp = q.getResultList();  
            //System.out.println("InstitucionesMacheadasLazyList: theQuery: " + query + ", rowCount: " + rowCount);
        } catch(Exception ex) {
            System.out.println("InstitucionesMacheadasLazyList:load: " + ex.getLocalizedMessage());
            setRowCount(0);
        }
        return institucionesTemp;
    }
    
    @Override
    public Object getRowKey(InstitucionTemp institucionTemp) {
        return institucionTemp.getIdInstitucion();
    }
    
    @Override
    public InstitucionTemp getRowData(String institucionTempId) {
        try {
            Integer id = Integer.valueOf(institucionTempId);
            for (InstitucionTemp institucionTemp : institucionesTemp) {
                if(id.equals(institucionTemp.getIdInstitucion())){
                    return institucionTemp;
                }
            }
            return null;
        }catch(Exception ex) {
            System.out.println("InstitucionesMacheadasLazyList:getRowData: " + ex.getLocalizedMessage());            
            return null;
        }
    }
}

