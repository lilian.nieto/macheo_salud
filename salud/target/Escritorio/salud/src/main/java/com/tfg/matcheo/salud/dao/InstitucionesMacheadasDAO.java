/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.matcheo.salud.dao;

import com.tfg.matcheo.salud.entity.InstitucionesMacheadas;

/**
 *
 * @author lilian
 */
public class InstitucionesMacheadasDAO extends GenericDAO<InstitucionesMacheadas>{
    private static final long serialVersionUID = 1L;

    public InstitucionesMacheadasDAO() {
        super(InstitucionesMacheadas.class);
    }
    
}
