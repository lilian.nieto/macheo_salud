/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author lilian
 */
@Entity
@Table(name = "instituciones_macheadas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstitucionesMacheadas.findAll", query = "SELECT i FROM InstitucionesMacheadas i"),
    @NamedQuery(name = "InstitucionesMacheadas.findByIdInstitucionesMacheadas", query = "SELECT i FROM InstitucionesMacheadas i WHERE i.idInstitucionesMacheadas = :idInstitucionesMacheadas"),
    @NamedQuery(name = "InstitucionesMacheadas.findByEstado", query = "SELECT i FROM InstitucionesMacheadas i WHERE i.estado = :estado"),
    @NamedQuery(name = "InstitucionesMacheadas.findByModo", query = "SELECT i FROM InstitucionesMacheadas i WHERE i.modo = :modo")})
public class InstitucionesMacheadas implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @SequenceGenerator(name="sequence1", sequenceName="sequence1", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sequence1")
    @Column(name = "id_instituciones_macheadas")
    private Integer idInstitucionesMacheadas;
    @Size(max = 20)
    @Column(name = "estado")
    private String estado;
    @Size(max = 20)
    @Column(name = "modo")
    private String modo;
    @JoinColumn(name = "institucion2", referencedColumnName = "id_institucion")
    @ManyToOne
    private InstitucionTemp institucion2;
    @JoinColumn(name = "institucion1", referencedColumnName = "id_institucion")
    @ManyToOne
    private InstitucionTemp institucion1;

    public InstitucionesMacheadas() {
    }

    public InstitucionesMacheadas(Integer idInstitucionesMacheadas) {
        this.idInstitucionesMacheadas = idInstitucionesMacheadas;
    }

    public Integer getIdInstitucionesMacheadas() {
        return idInstitucionesMacheadas;
    }

    public void setIdInstitucionesMacheadas(Integer idInstitucionesMacheadas) {
        this.idInstitucionesMacheadas = idInstitucionesMacheadas;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getModo() {
        return modo;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }

    public InstitucionTemp getInstitucion2() {
        return institucion2;
    }

    public void setInstitucion2(InstitucionTemp institucion2) {
        this.institucion2 = institucion2;
    }

    public InstitucionTemp getInstitucion1() {
        return institucion1;
    }

    public void setInstitucion1(InstitucionTemp institucion1) {
        this.institucion1 = institucion1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInstitucionesMacheadas != null ? idInstitucionesMacheadas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstitucionesMacheadas)) {
            return false;
        }
        InstitucionesMacheadas other = (InstitucionesMacheadas) object;
        if ((this.idInstitucionesMacheadas == null && other.idInstitucionesMacheadas != null) || (this.idInstitucionesMacheadas != null && !this.idInstitucionesMacheadas.equals(other.idInstitucionesMacheadas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.InstitucionesMacheadas[ idInstitucionesMacheadas=" + idInstitucionesMacheadas + " ]";
    }
    
}
