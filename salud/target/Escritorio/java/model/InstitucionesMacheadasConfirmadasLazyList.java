/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import beans.MacheoNombresInstitucionesEducativasBean;
import entity.InstitucionesMacheadas;
import entity.util.JpaUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author lilian
 */
@ManagedBean(name = "institucionesMacheadasConfirmadasLazyList")
@ViewScoped
public class InstitucionesMacheadasConfirmadasLazyList extends LazyDataModel<InstitucionesMacheadas> implements Serializable{
    
    private static final long serialVersionUID = 1L;    
    private List<InstitucionesMacheadas> institucionesMacheadas;    
    
    private int rowCount = 0;
    
    EntityManagerFactory emf = JpaUtil.getEntityManagerFactory();
    EntityManager em = emf.createEntityManager();
        
    @SuppressWarnings("unchecked")
    public static <T> T findBean(String beanName) {
         try{
            FacesContext context = FacesContext.getCurrentInstance();
            return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
        }catch(Exception e){
            return null;
        }
    }
    
    
    // el load del Lazy es el método principal y el que se sobreescribe con cada update en la interfaz
    @Override
    public List<InstitucionesMacheadas> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map<String, String> filters) {
        
        String departamento = "CONCEPCION";
        String codigoEstablecimiento1FiltroTabla2 = null;
        String codigoEstablecimiento2FiltroTabla2 = null;
        String nombreEstablecimiento1FiltroTabla2 = null;
        String nombreEstablecimiento2FiltroTabla2 = null;
        String tipoServicio1FiltroTabla2 = null;
        String tipoServicio2FiltroTabla2 = null;
        String distrito1FiltroTabla2 = null;
        MacheoNombresInstitucionesEducativasBean bean = findBean("macheoNombresInstitucionesEducativasBean");
        if(bean != null){
            if(bean.getSelectOneMenuDepartamento() != null){
                departamento = (String) bean.getSelectOneMenuDepartamento().getValue();
            }
            if(bean.getCodigoEstablecimiento1FiltroTabla2() != null){
                if (bean.getCodigoEstablecimiento1FiltroTabla2().getValue() != null){
                    codigoEstablecimiento1FiltroTabla2 = (String) bean.getCodigoEstablecimiento1FiltroTabla2().getValue();
                }
            }
            if(bean.getCodigoEstablecimiento2FiltroTabla2() != null){
                if (bean.getCodigoEstablecimiento2FiltroTabla2().getValue() != null){
                    codigoEstablecimiento2FiltroTabla2 = (String) bean.getCodigoEstablecimiento2FiltroTabla2().getValue();
                }
            }
            if(bean.getNombreEstablecimiento1FiltroTabla2() != null){
                if (bean.getNombreEstablecimiento1FiltroTabla2().getValue() != null){
                    nombreEstablecimiento1FiltroTabla2 = (String) bean.getNombreEstablecimiento1FiltroTabla2().getValue();
                }   
            }
            if(bean.getNombreEstablecimiento2FiltroTabla2() != null){
                if (bean.getNombreEstablecimiento2FiltroTabla2().getValue() != null){
                    nombreEstablecimiento2FiltroTabla2 = (String) bean.getNombreEstablecimiento2FiltroTabla2().getValue();
                }
            }
            if(bean.getTipoServicio1FiltroTabla2() != null){
                if (bean.getTipoServicio1FiltroTabla2().getValue() != null){
                    tipoServicio1FiltroTabla2 = (String) bean.getTipoServicio1FiltroTabla2().getValue();
                }
            }
            if(bean.getTipoServicio2FiltroTabla2() != null){
                if (bean.getTipoServicio2FiltroTabla2().getValue() != null){
                    tipoServicio2FiltroTabla2 = (String) bean.getTipoServicio2FiltroTabla2().getValue();
                }
            }
            if(bean.getDistrito1FiltroTabla2() != null){
                if (bean.getDistrito1FiltroTabla2().getValue() != null){
                    distrito1FiltroTabla2 = (String) bean.getDistrito1FiltroTabla2().getValue();
                }
            }
        }
        
        // <editor-fold defaultstate="collapsed" desc="Filtros">
        // definicion de filtros especificos menos filtro de fecha
        String queryFilter = null;
        String querySort = null;
        //if ( !filters.isEmpty() ) {
            if (codigoEstablecimiento1FiltroTabla2 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion1.codigoPrincipal) LIKE lower('%" + codigoEstablecimiento1FiltroTabla2 + "%') ";
                } else{
                    queryFilter = "lower(im.institucion1.codigoPrincipal) LIKE lower('%" + codigoEstablecimiento1FiltroTabla2 + "%') ";
                }
            }
            if (nombreEstablecimiento1FiltroTabla2 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion1.nombreInstitucionOriginal) LIKE lower('%" + nombreEstablecimiento1FiltroTabla2 + "%') ";
                } else{
                    queryFilter = "lower(im.institucion1.nombreInstitucionOriginal) LIKE lower('%" + nombreEstablecimiento1FiltroTabla2 + "%') ";
                }
            }
            if (tipoServicio1FiltroTabla2 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion1.codigoSecundario) LIKE lower('%" + tipoServicio1FiltroTabla2 + "%') ";
                } else{
                    queryFilter = "lower(im.institucion1.codigoSecundario) LIKE lower('%" + tipoServicio1FiltroTabla2 + "%') ";
                }
            }
            if (distrito1FiltroTabla2 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion1.nombreDistrito) LIKE lower('%" + distrito1FiltroTabla2 + "%') ";
                } else{
                    queryFilter = "lower(im.institucion1.nombreDistrito) LIKE lower('%" + distrito1FiltroTabla2 + "%') ";
                }
            }
            if (nombreEstablecimiento2FiltroTabla2 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion2.nombreInstitucionOriginal) LIKE lower('%" + nombreEstablecimiento2FiltroTabla2 + "%') ";
                } else{
                    queryFilter = "lower(im.institucion2.nombreInstitucionOriginal) LIKE lower('%" + nombreEstablecimiento2FiltroTabla2 + "%') ";
                }
            }
            /*if (filters.containsKey("institucion2.nombreDistrito")){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion2.nombreDistrito) LIKE lower('%" + filters.get("institucion2.nombreDistrito") + "%') ";
                } else{
                    queryFilter = "lower(im.institucion2.nombreDistrito) LIKE lower('%" + filters.get("institucion2.nombreDistrito") + "%') ";
                }
            }*/
            if (codigoEstablecimiento2FiltroTabla2 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion2.codigoPrincipal) LIKE lower('%" + codigoEstablecimiento2FiltroTabla2 + "%') ";
                } else{
                    queryFilter = "lower(im.institucion2.codigoPrincipal) LIKE lower('%" + codigoEstablecimiento2FiltroTabla2 + "%') ";
                }
            }
            if (tipoServicio2FiltroTabla2 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(im.institucion2.codigoSecundario) LIKE lower('%" + tipoServicio2FiltroTabla2 + "%') ";
                } else{
                    queryFilter = "lower(im.institucion2.codigoSecundario) LIKE lower('%" + tipoServicio2FiltroTabla2 + "%') ";
                }
            }

        //}      
        // definicion de order
        if(sortField != null) {
            querySort = "ORDER BY im." + sortField;
            querySort = querySort + (SortOrder.ASCENDING.equals(sortOrder) ? " ASC " : " DESC ");
        }
        // </editor-fold>

        String query;
        String queryCount;
        Query q;
            
        queryCount = "SELECT count(im) FROM InstitucionesMacheadas im "
                + "WHERE im.estado like 'CONFIRMADO' "
                + "AND im.institucion2.nombreDepartamento LIKE '" + departamento + "' ";
        query = "SELECT im FROM InstitucionesMacheadas im "
                + "WHERE im.estado like 'CONFIRMADO' "
                + "AND im.institucion2.nombreDepartamento LIKE '" + departamento + "' ";
        
        if (queryFilter != null){
            queryCount = queryCount + "AND " + queryFilter;
            query = query + "AND " + queryFilter;
        }
        if(querySort != null){
            query = query + querySort;
        }
        
        try {
            // query de cantidad de registros
            q = em.createQuery(queryCount);
            q.setHint("javax.persistence.cache.storeMode", "REFRESH");
            rowCount = ((Long) q.getSingleResult()).intValue(); 
            setRowCount(rowCount);
            
            // query de datos a desplegar
            q = em.createQuery(query);
            q.setFirstResult(startingAt);
            q.setMaxResults(maxPerPage);
            q.setHint("javax.persistence.cache.storeMode", "REFRESH");
            institucionesMacheadas = q.getResultList();  
            //System.out.println("InstitucionesMacheadasLazyList: theQuery: " + query + ", rowCount: " + rowCount);
        } catch(Exception ex) {
            System.out.println("InstitucionesMacheadasLazyList:load: " + ex.getLocalizedMessage());
            setRowCount(0);
        }
        return institucionesMacheadas;
    }
    
    @Override
    public Object getRowKey(InstitucionesMacheadas institucionMacheada) {
        return institucionMacheada.getIdInstitucionesMacheadas();
    }
    
    @Override
    public InstitucionesMacheadas getRowData(String institucionMacheadaId) {
        try {
            Integer id = Integer.valueOf(institucionMacheadaId);
            for (InstitucionesMacheadas institucionMacheada : institucionesMacheadas) {
                if(id.equals(institucionMacheada.getIdInstitucionesMacheadas())){
                    return institucionMacheada;
                }
            }
            return null;
        }catch(Exception ex) {
            System.out.println("InstitucionesMacheadasLazyList:getRowData: " + ex.getLocalizedMessage());            
            return null;
        }
    }
}
