/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import beans.MacheoNombresInstitucionesEducativasBean;
import entity.InstitucionTemp;
import entity.util.JpaUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author lilian
 */
@ManagedBean(name = "institucionesSinMachearLazyList")
@ViewScoped
public class InstitucionesSinMachearLazyList extends LazyDataModel<InstitucionTemp> implements Serializable{
    
    private static final long serialVersionUID = 1L;    
    private List<InstitucionTemp> institucionesTemp;    
    
    private int rowCount = 0;
    
    EntityManagerFactory emf = JpaUtil.getEntityManagerFactory();
    EntityManager em = emf.createEntityManager();
        
    @SuppressWarnings("unchecked")
    public static <T> T findBean(String beanName) {
        try{
            FacesContext context = FacesContext.getCurrentInstance();
            return (T) context.getApplication().evaluateExpressionGet(context, "#{" + beanName + "}", Object.class);
        }catch(Exception e){
            return null;
        }
    }
    
    // el load del Lazy es el método principal y el que se sobreescribe con cada update en la interfaz
    @Override
    public List<InstitucionTemp> load(int startingAt, int maxPerPage, String sortField, SortOrder sortOrder, Map<String, String> filters) {
        
        int archivo = 1;
        String departamento = "CONCEPCION";
        String codigoEstablecimientoFiltroTabla4 = null;
        String nombreEstablecimientoFiltroTabla4 = null;
        String tipoServicioFiltroTabla4 = null;
        String distritoFiltroTabla4 = null;
        MacheoNombresInstitucionesEducativasBean bean = findBean("macheoNombresInstitucionesEducativasBean");
        if(bean != null){
            if((Integer) bean.getSelectOneMenuArchivo().getValue() != null){
                archivo = (Integer) bean.getSelectOneMenuArchivo().getValue();
            }
            if((String) bean.getSelectOneMenuDepartamento().getValue() != null){
                departamento = (String) bean.getSelectOneMenuDepartamento().getValue();
            }
            if(bean.getCodigoEstablecimientoFiltroTabla4() != null){
                if (bean.getCodigoEstablecimientoFiltroTabla4().getValue() != null){
                    codigoEstablecimientoFiltroTabla4 = (String) bean.getCodigoEstablecimientoFiltroTabla4().getValue();
                }
            }
            if(bean.getNombreEstablecimientoFiltroTabla4() != null){
                if (bean.getNombreEstablecimientoFiltroTabla4().getValue() != null){
                    nombreEstablecimientoFiltroTabla4 = (String) bean.getNombreEstablecimientoFiltroTabla4().getValue();
                }
            }
            if(bean.getTipoServicioFiltroTabla4() != null){
                if (bean.getTipoServicioFiltroTabla4().getValue() != null){
                    tipoServicioFiltroTabla4 = (String) bean.getTipoServicioFiltroTabla4().getValue();
                }
            }
            if(bean.getDistritoFiltroTabla4() != null){
                if (bean.getDistritoFiltroTabla4().getValue() != null){
                    distritoFiltroTabla4 = (String) bean.getDistritoFiltroTabla4().getValue();
                }
            }
        }

        // <editor-fold defaultstate="collapsed" desc="Filtros">
        // definicion de filtros especificos menos filtro de fecha
        String queryFilter = null;
        String querySort = null;
        //if ( !filters.isEmpty() ) {
            if (codigoEstablecimientoFiltroTabla4 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.codigoPrincipal) LIKE lower('%" + codigoEstablecimientoFiltroTabla4 + "%') ";
                } else{
                    queryFilter = "lower(it.codigoPrincipal) LIKE lower('%" + codigoEstablecimientoFiltroTabla4 + "%') ";
                }
            }
            if (nombreEstablecimientoFiltroTabla4 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.nombreInstitucionOriginal) LIKE lower('%" + nombreEstablecimientoFiltroTabla4 + "%') ";
                } else{
                    queryFilter = "lower(it.nombreInstitucionOriginal) LIKE lower('%" + nombreEstablecimientoFiltroTabla4 + "%') ";
                }
            }
            if (tipoServicioFiltroTabla4 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.codigoSecundario) LIKE lower('%" + tipoServicioFiltroTabla4 + "%') ";
                } else{
                    queryFilter = "lower(it.codigoSecundario) LIKE lower('%" + tipoServicioFiltroTabla4 + "%') ";
                }
            }
            if (distritoFiltroTabla4 != null){
                if (queryFilter != null){
                    queryFilter = queryFilter + "AND ";
                    queryFilter = queryFilter + "lower(it.nombreDistrito) LIKE lower('%" + distritoFiltroTabla4 + "%') ";
                } else{
                    queryFilter = "lower(it.nombreDistrito) LIKE lower('%" + distritoFiltroTabla4 + "%') ";
                }
            }

        //}      
        // definicion de order
        if(sortField != null) {
            querySort = "ORDER BY it." + sortField;
            querySort = querySort + (SortOrder.ASCENDING.equals(sortOrder) ? " ASC " : " DESC ");
        }
        // </editor-fold>

        String query;
        String queryCount;
        Query q;
            
        queryCount = "SELECT count(it) FROM InstitucionTemp it "
                + "WHERE it.idInstitucion not in ";
        if(archivo == 2){
            queryCount = queryCount + "(SELECT im.institucion2.idInstitucion FROM InstitucionesMacheadas im) ";
        }else{
            queryCount = queryCount + "(SELECT im.institucion1.idInstitucion FROM InstitucionesMacheadas im) ";
        }
        queryCount = queryCount + "AND it.tipo = " + archivo + " "
                + "AND it.nombreDepartamento LIKE '" + departamento + "' ";
        
        query = "SELECT it FROM InstitucionTemp it "
                + "WHERE it.idInstitucion not in ";
        if(archivo == 2){
            query = query + "(SELECT im.institucion2.idInstitucion FROM InstitucionesMacheadas im) ";
        }else{
            query = query + "(SELECT im.institucion1.idInstitucion FROM InstitucionesMacheadas im) ";
        }
        query = query + "AND it.tipo = " + archivo + " "
                + "AND it.nombreDepartamento LIKE '" + departamento + "' ";
        
        if (queryFilter != null){
            queryCount = queryCount + "AND " + queryFilter;
            query = query + "AND " + queryFilter;
        }
        if(querySort != null){
            query = query + querySort;
        }
        
        try {
            // query de cantidad de registros
            q = em.createQuery(queryCount);
            q.setHint("javax.persistence.cache.storeMode", "REFRESH");
            rowCount = ((Long) q.getSingleResult()).intValue(); 
            setRowCount(rowCount);
            
            // query de datos a desplegar
            q = em.createQuery(query);
            q.setFirstResult(startingAt);
            q.setMaxResults(maxPerPage);
            q.setHint("javax.persistence.cache.storeMode", "REFRESH");
            institucionesTemp = q.getResultList();  
            //System.out.println("InstitucionesMacheadasLazyList: theQuery: " + query + ", rowCount: " + rowCount);
        } catch(Exception ex) {
            System.out.println("InstitucionesMacheadasLazyList:load: " + ex.getLocalizedMessage());
            setRowCount(0);
        }
        return institucionesTemp;
    }
    
    @Override
    public Object getRowKey(InstitucionTemp institucionTemp) {
        return institucionTemp.getIdInstitucion();
    }
    
    @Override
    public InstitucionTemp getRowData(String institucionTempId) {
        try {
            Integer id = Integer.valueOf(institucionTempId);
            for (InstitucionTemp institucionTemp : institucionesTemp) {
                if(id.equals(institucionTemp.getIdInstitucion())){
                    return institucionTemp;
                }
            }
            return null;
        }catch(Exception ex) {
            System.out.println("InstitucionesMacheadasLazyList:getRowData: " + ex.getLocalizedMessage());            
            return null;
        }
    }
}

