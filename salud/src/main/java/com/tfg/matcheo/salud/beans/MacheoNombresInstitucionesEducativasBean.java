/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tfg.matcheo.salud.beans;

import com.tfg.matcheo.salud.entity.InstitucionTemp;
import com.tfg.matcheo.salud.entity.InstitucionesMacheadas;
import com.tfg.matcheo.salud.entity.util.JpaUtil;
import com.tfg.matcheo.salud.facade.InstitucionTempFacade;
import com.tfg.matcheo.salud.facade.InstitucionesMacheadasFacade;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import com.tfg.matcheo.salud.model.InstitucionTempLazyList;
import com.tfg.matcheo.salud.model.InstitucionesMacheadasConfirmadasLazyList;
import com.tfg.matcheo.salud.model.InstitucionesMacheadasLazyList;
import com.tfg.matcheo.salud.model.InstitucionesMacheadasRechazadasLazyList;
import com.tfg.matcheo.salud.model.InstitucionesSinMachearLazyList;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author lilian
 */
@ManagedBean(name = "macheoNombresInstitucionesEducativasBean")
@ViewScoped
public class MacheoNombresInstitucionesEducativasBean extends AbstractMB implements Serializable {
 
    public MacheoNombresInstitucionesEducativasBean() {
        nombreDepartamento = "CONCEPCION";
        selectOneMenuDepartamento.setValue("CONCEPCION");
        archivo1Disabled = true;
        archivo2Disabled = true;
        archivo2AdjuntoDisabled = false;
        archivoPorDefecto = false;
    }
    
    public void printObjectivesToFile(String path, String mensaje){
    try {
      /* Open the file */
      FileOutputStream fos   = new FileOutputStream(path,true)     ;
      OutputStreamWriter osw = new OutputStreamWriter(fos)    ;
      BufferedWriter bw      = new BufferedWriter(osw)        ;
                        
      //for (int i = 0; i < solutionsList_.size(); i++) {
        //if (this.vector[i].getFitness()<1.0) {
        bw.write(mensaje);
        bw.newLine();
        //}
      //}
      
      /* Close the file */
      bw.close();
    }catch (IOException e) {
      //Configuration.logger_.severe("Error acceding to the file");
      e.printStackTrace();
    }
  } // printObjectivesToFile
    
    private InstitucionesMacheadas[] selectedInstitucionesMacheadaArray;

    public InstitucionesMacheadas[] getSelectedInstitucionesMacheadaArray() {
        return selectedInstitucionesMacheadaArray;
    }

    public void setSelectedInstitucionesMacheadaArray(InstitucionesMacheadas[] selectedInstitucionesMacheadaArray) {
        this.selectedInstitucionesMacheadaArray = selectedInstitucionesMacheadaArray;
    }
    
    private InstitucionTemp[] selectedInstitucionesSinMachearArray;

    public InstitucionTemp[] getSelectedInstitucionesSinMachearArray() {
        return selectedInstitucionesSinMachearArray;
    }

    public void setSelectedInstitucionesSinMachearArray(InstitucionTemp[] selectedInstitucionesSinMachearArray) {
        this.selectedInstitucionesSinMachearArray = selectedInstitucionesSinMachearArray;
    }
    
    private InstitucionTemp[] selectedInstitucionesOpcionesMachearArray;

    public InstitucionTemp[] getSelectedInstitucionesOpcionesMachearArray() {
        return selectedInstitucionesOpcionesMachearArray;
    }

    public void setSelectedInstitucionesOpcionesMachearArray(InstitucionTemp[] selectedInstitucionesOpcionesMachearArray) {
        this.selectedInstitucionesOpcionesMachearArray = selectedInstitucionesOpcionesMachearArray;
    }
    
    // <editor-fold defaultstate="collapsed" desc="Managed Component Definition">
    private int tabIndex = 0;
    private Map<String, Integer> columnas1Map = new HashMap<String, Integer>();
    private List<String> columnasList1 = new ArrayList<String>();
    private List<String> columnasList2 = new ArrayList<String>();
    private List<String> columnasList3 = new ArrayList<String>();
    private List<String> columnasList4 = new ArrayList<String>();
    private List<String> columnasList5 = new ArrayList<String>();
    private Map<String, Integer> columnas2Map = new HashMap<String, Integer>();
    private List<String> columnas2List1 = new ArrayList<String>();
    private List<String> columnas2List2 = new ArrayList<String>();
    private List<String> columnas2List3 = new ArrayList<String>();
    private List<String> columnas2List4 = new ArrayList<String>();
    private List<String> columnas2List5 = new ArrayList<String>();
    private String urlPrimerArchivo;
    private int archivo = 1;
    private String urlSegundoArchivo;
    private String destinationFile;
    private String destinationFilePrimerArchivo;
    private String destinationFileSegundoArchivo;
    private EntityManagerFactory emf = JpaUtil.getEntityManagerFactory();
    private EntityManager em = emf.createEntityManager();
    private String nombreDepartamento;
    private Long idDepartamento;
    private Long idInstitucionSelected;
    private InstitucionTempFacade institucionTempFacade;
    private InstitucionesMacheadasFacade institucionesMacheadasFacade;
    private String nombre1;
    private String nombre2;
    private String departamento1;
    private String departamento2;
    private String distrito1;
    private String distrito2;
    private String codigo1;
    private String codigo2;
    private String tipo1;
    private String tipo2;
    private SelectOneMenu selectOneMenuArchivo = new SelectOneMenu();
    private SelectOneMenu selectOneMenuDepartamento = new SelectOneMenu();
    private SelectOneMenu selectOneMenuNombre1 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuNombre2 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuDepartamento1 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuDepartamento2 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuDistrito1 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuDistrito2 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuCodigo1 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuCodigo2 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuEstado = new SelectOneMenu();
    private SelectOneMenu selectOneMenuTipo1 = new SelectOneMenu();
    private SelectOneMenu selectOneMenuTipo2 = new SelectOneMenu();
    Boolean archivo1Disabled;
    Boolean archivo2Disabled;
    Boolean archivo2AdjuntoDisabled;
    Boolean archivoPorDefecto;
    HtmlInputText codigoEstablecimiento1FiltroTabla1;
    HtmlInputText tipoServicio1FiltroTabla1;
    HtmlInputText distrito1FiltroTabla1;
    HtmlInputText nombreEstablecimiento1FiltroTabla1;
    HtmlInputText codigoEstablecimiento2FiltroTabla1;
    HtmlInputText tipoServicio2FiltroTabla1;
    HtmlInputText nombreEstablecimiento2FiltroTabla1;
    
    HtmlInputText codigoEstablecimiento1FiltroTabla2;
    HtmlInputText tipoServicio1FiltroTabla2;
    HtmlInputText distrito1FiltroTabla2;
    HtmlInputText nombreEstablecimiento1FiltroTabla2;
    HtmlInputText codigoEstablecimiento2FiltroTabla2;
    HtmlInputText tipoServicio2FiltroTabla2;
    HtmlInputText nombreEstablecimiento2FiltroTabla2;
    
    HtmlInputText codigoEstablecimiento1FiltroTabla3;
    HtmlInputText tipoServicio1FiltroTabla3;
    HtmlInputText distrito1FiltroTabla3;
    HtmlInputText nombreEstablecimiento1FiltroTabla3;
    HtmlInputText codigoEstablecimiento2FiltroTabla3;
    HtmlInputText tipoServicio2FiltroTabla3;
    HtmlInputText nombreEstablecimiento2FiltroTabla3;
    
    HtmlInputText codigoEstablecimientoFiltroTabla4;
    HtmlInputText tipoServicioFiltroTabla4;
    HtmlInputText distritoFiltroTabla4;
    HtmlInputText nombreEstablecimientoFiltroTabla4;
    
    HtmlInputText codigoEstablecimientoFiltroTabla5;
    HtmlInputText tipoServicioFiltroTabla5;
    HtmlInputText distritoFiltroTabla5;
    HtmlInputText nombreEstablecimientoFiltroTabla5;
    
    public HtmlInputText getNombreEstablecimientoFiltroTabla5() {
        return nombreEstablecimientoFiltroTabla5;
    }

    public void setNombreEstablecimientoFiltroTabla5(HtmlInputText nombreEstablecimientoFiltroTabla5) {
        this.nombreEstablecimientoFiltroTabla5 = nombreEstablecimientoFiltroTabla5;
    }
    
    public HtmlInputText getDistritoFiltroTabla5() {
        return distritoFiltroTabla5;
    }

    public void setDistritoFiltroTabla5(HtmlInputText distritoFiltroTabla5) {
        this.distritoFiltroTabla5 = distritoFiltroTabla5;
    }
    
    public HtmlInputText getTipoServicioFiltroTabla5() {
        return tipoServicioFiltroTabla5;
    }

    public void setTipoServicioFiltroTabla5(HtmlInputText tipoServicioFiltroTabla5) {
        this.tipoServicioFiltroTabla5 = tipoServicioFiltroTabla5;
    }
    
    public HtmlInputText getCodigoEstablecimientoFiltroTabla5() {
        return codigoEstablecimientoFiltroTabla5;
    }

    public void setCodigoEstablecimientoFiltroTabla5(HtmlInputText codigoEstablecimientoFiltroTabla5) {
        this.codigoEstablecimientoFiltroTabla5 = codigoEstablecimientoFiltroTabla5;
    }
    
    //***
    
    public HtmlInputText getNombreEstablecimientoFiltroTabla4() {
        return nombreEstablecimientoFiltroTabla4;
    }

    public void setNombreEstablecimientoFiltroTabla4(HtmlInputText nombreEstablecimientoFiltroTabla4) {
        this.nombreEstablecimientoFiltroTabla4 = nombreEstablecimientoFiltroTabla4;
    }
    
    public HtmlInputText getDistritoFiltroTabla4() {
        return distritoFiltroTabla4;
    }

    public void setDistritoFiltroTabla4(HtmlInputText distritoFiltroTabla4) {
        this.distritoFiltroTabla4 = distritoFiltroTabla4;
    }
    
    public HtmlInputText getTipoServicioFiltroTabla4() {
        return tipoServicioFiltroTabla4;
    }

    public void setTipoServicioFiltroTabla4(HtmlInputText tipoServicioFiltroTabla4) {
        this.tipoServicioFiltroTabla4 = tipoServicioFiltroTabla4;
    }
    
    public HtmlInputText getCodigoEstablecimientoFiltroTabla4() {
        return codigoEstablecimientoFiltroTabla4;
    }

    public void setCodigoEstablecimientoFiltroTabla4(HtmlInputText codigoEstablecimientoFiltroTabla4) {
        this.codigoEstablecimientoFiltroTabla4 = codigoEstablecimientoFiltroTabla4;
    }
    
    //***
    
    public HtmlInputText getNombreEstablecimiento2FiltroTabla3() {
        return nombreEstablecimiento2FiltroTabla3;
    }

    public void setNombreEstablecimiento2FiltroTabla3(HtmlInputText nombreEstablecimiento2FiltroTabla3) {
        this.nombreEstablecimiento2FiltroTabla3 = nombreEstablecimiento2FiltroTabla3;
    }
    
    public HtmlInputText getTipoServicio2FiltroTabla3() {
        return tipoServicio2FiltroTabla3;
    }

    public void setTipoServicio2FiltroTabla3(HtmlInputText tipoServicio2FiltroTabla3) {
        this.tipoServicio2FiltroTabla3 = tipoServicio2FiltroTabla3;
    }
    
    public HtmlInputText getCodigoEstablecimiento2FiltroTabla3() {
        return codigoEstablecimiento2FiltroTabla3;
    }

    public void setCodigoEstablecimiento2FiltroTabla3(HtmlInputText codigoEstablecimiento2FiltroTabla3) {
        this.codigoEstablecimiento2FiltroTabla3 = codigoEstablecimiento2FiltroTabla3;
    }
    
    public HtmlInputText getNombreEstablecimiento1FiltroTabla3() {
        return nombreEstablecimiento1FiltroTabla3;
    }

    public void setNombreEstablecimiento1FiltroTabla3(HtmlInputText nombreEstablecimiento1FiltroTabla3) {
        this.nombreEstablecimiento1FiltroTabla3 = nombreEstablecimiento1FiltroTabla3;
    }
    
    public HtmlInputText getDistrito1FiltroTabla3() {
        return distrito1FiltroTabla3;
    }

    public void setDistrito1FiltroTabla3(HtmlInputText distrito1FiltroTabla3) {
        this.distrito1FiltroTabla3 = distrito1FiltroTabla3;
    }
    
    public HtmlInputText getTipoServicio1FiltroTabla3() {
        return tipoServicio1FiltroTabla3;
    }

    public void setTipoServicio1FiltroTabla3(HtmlInputText tipoServicio1FiltroTabla3) {
        this.tipoServicio1FiltroTabla3 = tipoServicio1FiltroTabla3;
    }
    
    public HtmlInputText getCodigoEstablecimiento1FiltroTabla3() {
        return codigoEstablecimiento1FiltroTabla3;
    }

    public void setCodigoEstablecimiento1FiltroTabla3(HtmlInputText codigoEstablecimiento1FiltroTabla3) {
        this.codigoEstablecimiento1FiltroTabla3 = codigoEstablecimiento1FiltroTabla3;
    }
    
    //***
    public HtmlInputText getNombreEstablecimiento2FiltroTabla2() {
        return nombreEstablecimiento2FiltroTabla2;
    }

    public void setNombreEstablecimiento2FiltroTabla2(HtmlInputText nombreEstablecimiento2FiltroTabla2) {
        this.nombreEstablecimiento2FiltroTabla2 = nombreEstablecimiento2FiltroTabla2;
    }
    
    public HtmlInputText getTipoServicio2FiltroTabla2() {
        return tipoServicio2FiltroTabla2;
    }

    public void setTipoServicio2FiltroTabla2(HtmlInputText tipoServicio2FiltroTabla2) {
        this.tipoServicio2FiltroTabla2 = tipoServicio2FiltroTabla2;
    }
    
    public HtmlInputText getCodigoEstablecimiento2FiltroTabla2() {
        return codigoEstablecimiento2FiltroTabla2;
    }

    public void setCodigoEstablecimiento2FiltroTabla2(HtmlInputText codigoEstablecimiento2FiltroTabla2) {
        this.codigoEstablecimiento2FiltroTabla2 = codigoEstablecimiento2FiltroTabla2;
    }
    
    public HtmlInputText getNombreEstablecimiento1FiltroTabla2() {
        return nombreEstablecimiento1FiltroTabla2;
    }

    public void setNombreEstablecimiento1FiltroTabla2(HtmlInputText nombreEstablecimiento1FiltroTabla2) {
        this.nombreEstablecimiento1FiltroTabla2 = nombreEstablecimiento1FiltroTabla2;
    }
    
    public HtmlInputText getDistrito1FiltroTabla2() {
        return distrito1FiltroTabla2;
    }

    public void setDistrito1FiltroTabla2(HtmlInputText distrito1FiltroTabla2) {
        this.distrito1FiltroTabla2 = distrito1FiltroTabla2;
    }
    
    public HtmlInputText getTipoServicio1FiltroTabla2() {
        return tipoServicio1FiltroTabla2;
    }

    public void setTipoServicio1FiltroTabla2(HtmlInputText tipoServicio1FiltroTabla2) {
        this.tipoServicio1FiltroTabla2 = tipoServicio1FiltroTabla2;
    }
    
    public HtmlInputText getCodigoEstablecimiento1FiltroTabla2() {
        return codigoEstablecimiento1FiltroTabla2;
    }

    public void setCodigoEstablecimiento1FiltroTabla2(HtmlInputText codigoEstablecimiento1FiltroTabla2) {
        this.codigoEstablecimiento1FiltroTabla2 = codigoEstablecimiento1FiltroTabla2;
    }
    
    //***
    
    public HtmlInputText getNombreEstablecimiento2FiltroTabla1() {
        return nombreEstablecimiento2FiltroTabla1;
    }

    public void setNombreEstablecimiento2FiltroTabla1(HtmlInputText nombreEstablecimiento2FiltroTabla1) {
        this.nombreEstablecimiento2FiltroTabla1 = nombreEstablecimiento2FiltroTabla1;
    }
    
    public HtmlInputText getTipoServicio2FiltroTabla1() {
        return tipoServicio2FiltroTabla1;
    }

    public void setTipoServicio2FiltroTabla1(HtmlInputText tipoServicio2FiltroTabla1) {
        this.tipoServicio2FiltroTabla1 = tipoServicio2FiltroTabla1;
    }
    
    public HtmlInputText getCodigoEstablecimiento2FiltroTabla1() {
        return codigoEstablecimiento2FiltroTabla1;
    }

    public void setCodigoEstablecimiento2FiltroTabla1(HtmlInputText codigoEstablecimiento2FiltroTabla1) {
        this.codigoEstablecimiento2FiltroTabla1 = codigoEstablecimiento2FiltroTabla1;
    }
    
    public HtmlInputText getNombreEstablecimiento1FiltroTabla1() {
        return nombreEstablecimiento1FiltroTabla1;
    }

    public void setNombreEstablecimiento1FiltroTabla1(HtmlInputText nombreEstablecimiento1FiltroTabla1) {
        this.nombreEstablecimiento1FiltroTabla1 = nombreEstablecimiento1FiltroTabla1;
    }
    
    public HtmlInputText getDistrito1FiltroTabla1() {
        return distrito1FiltroTabla1;
    }

    public void setDistrito1FiltroTabla1(HtmlInputText distrito1FiltroTabla1) {
        this.distrito1FiltroTabla1 = distrito1FiltroTabla1;
    }
    
    public HtmlInputText getTipoServicio1FiltroTabla1() {
        return tipoServicio1FiltroTabla1;
    }

    public void setTipoServicio1FiltroTabla1(HtmlInputText tipoServicio1FiltroTabla1) {
        this.tipoServicio1FiltroTabla1 = tipoServicio1FiltroTabla1;
    }
    
    public HtmlInputText getCodigoEstablecimiento1FiltroTabla1() {
        return codigoEstablecimiento1FiltroTabla1;
    }

    public void setCodigoEstablecimiento1FiltroTabla1(HtmlInputText codigoEstablecimiento1FiltroTabla1) {
        this.codigoEstablecimiento1FiltroTabla1 = codigoEstablecimiento1FiltroTabla1;
    }
    
    public int getTabIndex() {
        return tabIndex;
    }

    public void setTabIndex(int tabIndex) {
        this.tabIndex = tabIndex;
    }

    public Boolean getArchivoPorDefecto() {
        return archivoPorDefecto;
    }

    public void setArchivoPorDefecto(Boolean archivoPorDefecto) {
        this.archivoPorDefecto = archivoPorDefecto;
    }

    public Boolean getArchivo1Disabled() {
        return archivo1Disabled;
    }

    public void setArchivo1Disabled(Boolean archivo1Disabled) {
        this.archivo1Disabled = archivo1Disabled;
    }

    public Boolean getArchivo2Disabled() {
        return archivo2Disabled;
    }

    public void setArchivo2Disabled(Boolean archivo2Disabled) {
        this.archivo2Disabled = archivo2Disabled;
    }

    public Boolean getArchivo2AdjuntoDisabled() {
        return archivo2AdjuntoDisabled;
    }

    public void setArchivo2AdjuntoDisabled(Boolean archivo2AdjuntoDisabled) {
        this.archivo2AdjuntoDisabled = archivo2AdjuntoDisabled;
    }

    public List<String> getColumnasList1() {
        return columnasList1;
    }

    public void setColumnasList1(List<String> columnasList1) {
        this.columnasList1 = columnasList1;
    }

    public List<String> getColumnasList2() {
        return columnasList2;
    }

    public void setColumnasList2(List<String> columnasList2) {
        this.columnasList2 = columnasList2;
    }

    public List<String> getColumnasList3() {
        return columnasList3;
    }

    public void setColumnasList3(List<String> columnasList3) {
        this.columnasList3 = columnasList3;
    }

    public List<String> getColumnasList4() {
        return columnasList4;
    }

    public void setColumnasList4(List<String> columnasList4) {
        this.columnasList4 = columnasList4;
    }
    
    public List<String> getColumnasList5() {
        return columnasList5;
    }

    public void setColumnasList5(List<String> columnasList5) {
        this.columnasList5 = columnasList5;
    }

    public List<String> getColumnas2List1() {
        return columnas2List1;
    }

    public void setColumnas2List(List<String> columnas2List1) {
        this.columnas2List1 = columnas2List1;
    }

    public List<String> getColumnas2List2() {
        return columnas2List2;
    }

    public void setColumnas2List2(List<String> columnas2List2) {
        this.columnas2List2 = columnas2List2;
    }

    public List<String> getColumnas2List3() {
        return columnas2List3;
    }

    public void setColumnas2List3(List<String> columnas2List3) {
        this.columnas2List3 = columnas2List3;
    }

    public List<String> getColumnas2List4() {
        return columnas2List4;
    }

    public void setColumnas2List4(List<String> columnas2List4) {
        this.columnas2List4 = columnas2List4;
    }
    
    public List<String> getColumnas2List5() {
        return columnas2List5;
    }

    public void setColumnas2List5(List<String> columnas2List5) {
        this.columnas2List5 = columnas2List5;
    }

    public Map<String, Integer> getColumnas1Map() {
        return columnas1Map;
    }

    public void setColumnas1Map(Map<String, Integer> columnas1Map) {
        this.columnas1Map = columnas1Map;
    }

    public Map<String, Integer> getColumnas2Map() {
        return columnas2Map;
    }

    public void setColumnas2Map(Map<String, Integer> columnas2Map) {
        this.columnas2Map = columnas2Map;
    }

    public String getUrlPrimerArchivo() {
        return urlPrimerArchivo;
    }

    public void setUrlPrimerArchivo(String urlPrimerArchivo) {
        this.urlPrimerArchivo = urlPrimerArchivo;
    }
    
    public int getArchivo() {
        return archivo;
    }

    public void setArchivo(int archivo) {
        this.archivo = archivo;
    }

    public String getUrlSegundoArchivo() {
        return urlSegundoArchivo;
    }

    public void setUrlSegundoArchivo(String urlSegundoArchivo) {
        this.urlSegundoArchivo = urlSegundoArchivo;
    }

    public String getDestinationFile() {
        return destinationFile;
    }

    public void setDestinationFile(String destinationFile) {
        this.destinationFile = destinationFile;
    }

    public String getDestinationFilePrimerArchivo() {
        return destinationFilePrimerArchivo;
    }

    public void setDestinationFilePrimerArchivo(String destinationFilePrimerArchivo) {
        this.destinationFilePrimerArchivo = destinationFilePrimerArchivo;
    }

    public String getDestinationFileSegundoArchivo() {
        return destinationFileSegundoArchivo;
    }

    public void setDestinationFileSegundoArchivo(String destinationFileSegundoArchivo) {
        this.destinationFileSegundoArchivo = destinationFileSegundoArchivo;
    }
    
    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getDepartamento1() {
        return departamento1;
    }

    public void setDepartamento1(String departamento1) {
        this.departamento1 = departamento1;
    }

    public String getDepartamento2() {
        return departamento2;
    }

    public void setDepartamento2(String departamento2) {
        this.departamento2 = departamento2;
    }

    public String getDistrito1() {
        return distrito1;
    }

    public void setDistrito1(String distrito1) {
        this.distrito1 = distrito1;
    }

    public String getDistrito2() {
        return distrito2;
    }

    public void setDistrito2(String distrito2) {
        this.distrito2 = distrito2;
    }
    
    public String getTipo1() {
        return tipo1;
    }

    public void setTipo1(String tipo1) {
        this.tipo1 = tipo1;
    }
    
    public String getTipo2() {
        return tipo2;
    }

    public void setTipo2(String tipo2) {
        this.tipo2 = tipo2;
    }

    public String getCodigo1() {
        return codigo1;
    }

    public void setCodigo1(String codigo1) {
        this.codigo1 = codigo1;
    }

    public String getCodigo2() {
        return codigo2;
    }

    public void setCodigo2(String codigo2) {
        this.codigo2 = codigo2;
    }

    public EntityManager getEm() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public EntityManagerFactory getEmf() {
        return emf;
    }

    public void setEmf(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public String getNombreDepartamento() {
        return nombreDepartamento;
    }

    public void setNombreDepartamento(String nombreDepartamento) {
        this.nombreDepartamento = nombreDepartamento;
    }

    public Long getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Long idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Long getIdInstitucionSelected() {
        return idInstitucionSelected;
    }

    public void setIdInstitucionSelected(Long idInstitucionSelected) {
        this.idInstitucionSelected = idInstitucionSelected;
    }

    public InstitucionTempFacade getInstitucionTempFacade() {
        if (institucionTempFacade == null) {
            institucionTempFacade = new InstitucionTempFacade();
        }
        return institucionTempFacade;
    }

    public InstitucionesMacheadasFacade getInstitucionesMacheadasFacade() {
        if (institucionesMacheadasFacade == null) {
            institucionesMacheadasFacade = new InstitucionesMacheadasFacade();
        }
        return institucionesMacheadasFacade;
    }

    public SelectOneMenu getSelectOneMenuDepartamento() {
        return selectOneMenuDepartamento;
    }

    public void setSelectOneMenuDepartamento(SelectOneMenu selectOneMenuDepartamento) {
        this.selectOneMenuDepartamento = selectOneMenuDepartamento;
    }
    
    public SelectOneMenu getSelectOneMenuArchivo() {
        return selectOneMenuArchivo;
    }

    public void setSelectOneMenuArchivo(SelectOneMenu selectOneMenuArchivo) {
        this.selectOneMenuArchivo = selectOneMenuArchivo;
    }
    
    public SelectOneMenu getSelectOneMenuEstado() {
        return selectOneMenuEstado;
    }

    public void setSelectOneMenuEstado(SelectOneMenu selectOneMenuEstado) {
        this.selectOneMenuEstado = selectOneMenuEstado;
    }

    public SelectOneMenu getSelectOneMenuNombre1() {
        return selectOneMenuNombre1;
    }

    public void setSelectOneMenuNombre1(SelectOneMenu selectOneMenuNombre1) {
        this.selectOneMenuNombre1 = selectOneMenuNombre1;
    }

    public SelectOneMenu getSelectOneMenuNombre2() {
        return selectOneMenuNombre2;
    }

    public void setSelectOneMenuNombre2(SelectOneMenu selectOneMenuNombre2) {
        this.selectOneMenuNombre2 = selectOneMenuNombre2;
    }

    public SelectOneMenu getSelectOneMenuDepartamento1() {
        return selectOneMenuDepartamento1;
    }

    public void setSelectOneMenuDepartamento1(SelectOneMenu selectOneMenuDepartamento1) {
        this.selectOneMenuDepartamento1 = selectOneMenuDepartamento1;
    }

    public SelectOneMenu getSelectOneMenuDepartamento2() {
        return selectOneMenuDepartamento2;
    }

    public void setSelectOneMenuDepartamento2(SelectOneMenu selectOneMenuDepartamento2) {
        this.selectOneMenuDepartamento2 = selectOneMenuDepartamento2;
    }

    public SelectOneMenu getSelectOneMenuDistrito1() {
        return selectOneMenuDistrito1;
    }

    public void setSelectOneMenuDistrito1(SelectOneMenu selectOneMenuDistrito1) {
        this.selectOneMenuDistrito1 = selectOneMenuDistrito1;
    }

    public SelectOneMenu getSelectOneMenuDistrito2() {
        return selectOneMenuDistrito2;
    }

    public void setSelectOneMenuDistrito2(SelectOneMenu selectOneMenuDistrito2) {
        this.selectOneMenuDistrito2 = selectOneMenuDistrito2;
    }
    
    public SelectOneMenu getSelectOneMenuTipo1() {
        return selectOneMenuTipo1;
    }

    public void setSelectOneMenuTipo1(SelectOneMenu selectOneMenuTipo1) {
        this.selectOneMenuTipo1 = selectOneMenuTipo1;
    }
    
    public SelectOneMenu getSelectOneMenuTipo2() {
        return selectOneMenuTipo2;
    }

    public void setSelectOneMenuTipo2(SelectOneMenu selectOneMenuTipo2) {
        this.selectOneMenuTipo2 = selectOneMenuTipo2;
    }

    public SelectOneMenu getSelectOneMenuCodigo1() {
        return selectOneMenuCodigo1;
    }

    public void setSelectOneMenuCodigo1(SelectOneMenu selectOneMenuCodigo1) {
        this.selectOneMenuCodigo1 = selectOneMenuCodigo1;
    }

    public SelectOneMenu getSelectOneMenuCodigo2() {
        return selectOneMenuCodigo2;
    }

    public void setSelectOneMenuCodigo2(SelectOneMenu selectOneMenuCodigo2) {
        this.selectOneMenuCodigo2 = selectOneMenuCodigo2;
    }

    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="método para subir archivo al servidor: (update)">
    public String handleFileUploadUpdatePrimerArchivo(FileUploadEvent event) throws IOException, NamingException, NotSupportedException, SystemException, RollbackException, HeuristicMixedException, HeuristicRollbackException, javax.transaction.NotSupportedException, javax.transaction.RollbackException {
        try {
            copyFilePrimerArchivo(event.getFile().getFileName(), event.getFile().getInputstream());
            this.setUrlPrimerArchivo(event.getFile().getFileName());
            FacesMessage msg = new FacesMessage("Correcto", event.getFile().getFileName() + " se ha adjuntado correctamente.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            archivo1Disabled = false;
            return "index?faces-redirect=true";
        } catch (IOException e) {
            System.out.println(e.getMessage());
            FacesMessage error = new FacesMessage(FacesMessage.SEVERITY_ERROR, "El archivo no ha sido subido", "");
            FacesContext.getCurrentInstance().addMessage(null, error);
            return "index?faces-redirect=true";
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="método para subir archivo al servidor: (update)">
    public String handleFileUploadUpdateSegundoArchivo(FileUploadEvent event) throws IOException, NamingException, NotSupportedException, SystemException, RollbackException, HeuristicMixedException, HeuristicRollbackException, javax.transaction.NotSupportedException, javax.transaction.RollbackException {
        try {
            copyFileSegundoArchivo(event.getFile().getFileName(), event.getFile().getInputstream());
            this.setUrlSegundoArchivo(event.getFile().getFileName());
            FacesMessage msg = new FacesMessage("Correcto", event.getFile().getFileName() + " se ha adjuntado correctamente.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
            archivo2Disabled = false;
            return "index?faces-redirect=true";
        } catch (IOException e) {
            System.out.println(e.getMessage());
            FacesMessage error = new FacesMessage(FacesMessage.SEVERITY_ERROR, "El archivo no ha sido subido", "");
            FacesContext.getCurrentInstance().addMessage(null, error);
            return "index?faces-redirect=true";
        }
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="método para copiar el archivo al servidor">

    public void copyFilePrimerArchivo(String fileName, InputStream in) {

        try {
            FacesContext context2 = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) context2.getExternalContext().getContext();

            //this.setDestinationFilePrimerArchivo(servletContext.getRealPath("/" + fileName));
            this.setDestinationFilePrimerArchivo(fileName);
                        
            /*File folder = new File(servletContext.getRealPath("/documentaciones/notas/"));
            if (!folder.exists()) { // si no existe la carpeta, la creamos
                folder.mkdir();
            }*/
            // verificacion si el archivo que vamos a subir ya existe
            File file = new File(destinationFilePrimerArchivo);
            /*if (file.exists()) { // si existe el archivo, modificamos el nombre antes de subir
             // separar nombre de archivo y extension
             String extension = "", fileSinExtension = "";

             int i = file.getName().lastIndexOf('.');
             if (i > 0) {
             extension = file.getName().substring(i);
             fileSinExtension = file.getName().substring(0, i);
             }
             String fileRenombrado = "";
             fileRenombrado = fileSinExtension + "_copia" + extension;

             file.renameTo(new File(servletContext.getRealPath("/documentaciones/notas/" + fileRenombrado)));
             }*/
            OutputStream out = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();

            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            br = new BufferedReader(new FileReader(destinationFilePrimerArchivo));
            columnasList1 = new ArrayList<String>();
            columnasList2 = new ArrayList<String>();
            columnasList3 = new ArrayList<String>();
            columnasList4 = new ArrayList<String>();
            columnasList5 = new ArrayList<String>();
            columnas1Map = new HashMap<String, Integer>();
            while ((line = br.readLine()) != null) {
                String[] resultado = line.split(cvsSplitBy);
                for (int i = 0; i < resultado.length; i++) {
                    columnasList1.add(resultado[i]);
                    columnasList2.add(resultado[i]);
                    columnasList3.add(resultado[i]);
                    columnasList4.add(resultado[i]);
                    columnasList5.add(resultado[i]);
                    columnas1Map.put(resultado[i], i);
                }
                break;
            }
            br.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="método para copiar el archivo al servidor">
    public void copyFileSegundoArchivo(String fileName, InputStream in) {

        try {
            // write the inputStream to a FileOutputStream
            FacesContext context2 = FacesContext.getCurrentInstance();
            ServletContext servletContext = (ServletContext) context2.getExternalContext().getContext();

            //this.setDestinationFileSegundoArchivo(servletContext.getRealPath("/" + fileName));
            this.setDestinationFileSegundoArchivo(fileName);
            File folder = new File(servletContext.getRealPath("/"));
            if (!folder.exists()) { // si no existe la carpeta, la creamos
                folder.mkdir();
            }
            // verificacion si el archivo que vamos a subir ya existe
            File file = new File(destinationFileSegundoArchivo);
            /*if (file.exists()) { // si existe el archivo, modificamos el nombre antes de subir
             // separar nombre de archivo y extension
             String extension = "", fileSinExtension = "";

             int i = file.getName().lastIndexOf('.');
             if (i > 0) {
             extension = file.getName().substring(i);
             fileSinExtension = file.getName().substring(0, i);
             }
             String fileRenombrado = "";
             fileRenombrado = fileSinExtension + "_copia" + extension;

             file.renameTo(new File(servletContext.getRealPath("/documentaciones/notas/" + fileRenombrado)));
             }*/
            OutputStream out = new FileOutputStream(file);
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();

            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            br = new BufferedReader(new FileReader(destinationFileSegundoArchivo));
            columnas2List1 = new ArrayList<String>();
            columnas2List2 = new ArrayList<String>();
            columnas2List3 = new ArrayList<String>();
            columnas2List4 = new ArrayList<String>();
            columnas2List5 = new ArrayList<String>();
            columnas2Map = new HashMap<String, Integer>();
            while ((line = br.readLine()) != null) {
                String[] resultado = line.split(cvsSplitBy);
                for (int i = 0; i < resultado.length; i++) {
                    columnas2List1.add(resultado[i]);
                    columnas2List2.add(resultado[i]);
                    columnas2List3.add(resultado[i]);
                    columnas2List4.add(resultado[i]);
                    columnas2List5.add(resultado[i]);
                    columnas2Map.put(resultado[i], i);
                }
                break;
            }
            br.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Registrar Datos">
    Integer contadorMacheador;
    
    public void refrescarPestana1() {
        tabIndex = 0;
        codigoEstablecimiento1FiltroTabla1.setValue(null);
        tipoServicio1FiltroTabla1.setValue(null);
        distrito1FiltroTabla1.setValue(null);
        nombreEstablecimiento1FiltroTabla1.setValue(null);
        codigoEstablecimiento2FiltroTabla1.setValue(null);
        tipoServicio2FiltroTabla1.setValue(null);
        nombreEstablecimiento2FiltroTabla1.setValue(null);   
    }
    
    public void refrescarPestana2() {
        tabIndex = 1;
        codigoEstablecimiento1FiltroTabla2.setValue(null);
        tipoServicio1FiltroTabla2.setValue(null);
        distrito1FiltroTabla2.setValue(null);
        nombreEstablecimiento1FiltroTabla2.setValue(null);
        codigoEstablecimiento2FiltroTabla2.setValue(null);
        tipoServicio2FiltroTabla2.setValue(null);
        nombreEstablecimiento2FiltroTabla2.setValue(null);   
    }
    
    public void refrescarPestana3() {
        tabIndex = 2;
        codigoEstablecimiento1FiltroTabla3.setValue(null);
        tipoServicio1FiltroTabla3.setValue(null);
        distrito1FiltroTabla3.setValue(null);
        nombreEstablecimiento1FiltroTabla3.setValue(null);
        codigoEstablecimiento2FiltroTabla3.setValue(null);
        tipoServicio2FiltroTabla3.setValue(null);
        nombreEstablecimiento2FiltroTabla3.setValue(null);   
    }
    
    public void refrescarPestana4Tabla1() {
        tabIndex = 3;
        codigoEstablecimientoFiltroTabla4.setValue(null);
        tipoServicioFiltroTabla4.setValue(null);
        distritoFiltroTabla4.setValue(null);
        nombreEstablecimientoFiltroTabla4.setValue(null);
    }
    
    public void refrescarPestana4Tabla2() {
        tabIndex = 3;
        codigoEstablecimientoFiltroTabla5.setValue(null);
        tipoServicioFiltroTabla5.setValue(null);
        distritoFiltroTabla5.setValue(null);
        nombreEstablecimientoFiltroTabla5.setValue(null);
    }
    
    public void limpiarFiltros() {
        codigoEstablecimiento1FiltroTabla1.setValue(null);
        tipoServicio1FiltroTabla1.setValue(null);
        distrito1FiltroTabla1.setValue(null);
        nombreEstablecimiento1FiltroTabla1.setValue(null);
        codigoEstablecimiento2FiltroTabla1.setValue(null);
        tipoServicio2FiltroTabla1.setValue(null);
        nombreEstablecimiento2FiltroTabla1.setValue(null);   
        codigoEstablecimiento1FiltroTabla2.setValue(null);
        tipoServicio1FiltroTabla2.setValue(null);
        distrito1FiltroTabla2.setValue(null);
        nombreEstablecimiento1FiltroTabla2.setValue(null);
        codigoEstablecimiento2FiltroTabla2.setValue(null);
        tipoServicio2FiltroTabla2.setValue(null);
        nombreEstablecimiento2FiltroTabla2.setValue(null);   
        codigoEstablecimiento1FiltroTabla3.setValue(null);
        tipoServicio1FiltroTabla3.setValue(null);
        distrito1FiltroTabla3.setValue(null);
        nombreEstablecimiento1FiltroTabla3.setValue(null);
        codigoEstablecimiento2FiltroTabla3.setValue(null);
        tipoServicio2FiltroTabla3.setValue(null);
        nombreEstablecimiento2FiltroTabla3.setValue(null);   
        codigoEstablecimientoFiltroTabla4.setValue(null);
        tipoServicioFiltroTabla4.setValue(null);
        distritoFiltroTabla4.setValue(null);
        nombreEstablecimientoFiltroTabla4.setValue(null);
        codigoEstablecimientoFiltroTabla5.setValue(null);
        tipoServicioFiltroTabla5.setValue(null);
        distritoFiltroTabla5.setValue(null);
        nombreEstablecimientoFiltroTabla5.setValue(null);
    }
    
    public void onTabChange(TabChangeEvent event) {
        if(event.getTab().getId().compareTo("tabViewDialog1")==0){
            codigoEstablecimiento1FiltroTabla1.setValue(null);
            tipoServicio1FiltroTabla1.setValue(null);
            distrito1FiltroTabla1.setValue(null);
            nombreEstablecimiento1FiltroTabla1.setValue(null);
            codigoEstablecimiento2FiltroTabla1.setValue(null);
            tipoServicio2FiltroTabla1.setValue(null);
            nombreEstablecimiento2FiltroTabla1.setValue(null);
        }else if(event.getTab().getId().compareTo("tabViewDialog2")==0){
            codigoEstablecimiento1FiltroTabla2.setValue(null);
            tipoServicio1FiltroTabla2.setValue(null);
            distrito1FiltroTabla2.setValue(null);
            nombreEstablecimiento1FiltroTabla2.setValue(null);
            codigoEstablecimiento2FiltroTabla2.setValue(null);
            tipoServicio2FiltroTabla2.setValue(null);
            nombreEstablecimiento2FiltroTabla2.setValue(null);
        }else if(event.getTab().getId().compareTo("tabViewDialog3")==0){
            codigoEstablecimiento1FiltroTabla3.setValue(null);
            tipoServicio1FiltroTabla3.setValue(null);
            distrito1FiltroTabla3.setValue(null);
            nombreEstablecimiento1FiltroTabla3.setValue(null);
            codigoEstablecimiento2FiltroTabla3.setValue(null);
            tipoServicio2FiltroTabla3.setValue(null);
            nombreEstablecimiento2FiltroTabla3.setValue(null);   
        }else if(event.getTab().getId().compareTo("tabViewDialog4")==0){
            codigoEstablecimientoFiltroTabla4.setValue(null);
            tipoServicioFiltroTabla4.setValue(null);
            distritoFiltroTabla4.setValue(null);
            nombreEstablecimientoFiltroTabla4.setValue(null);
            codigoEstablecimientoFiltroTabla5.setValue(null);
            tipoServicioFiltroTabla5.setValue(null);
            distritoFiltroTabla5.setValue(null);
            nombreEstablecimientoFiltroTabla5.setValue(null);
        }
    }
    
    public void matchearRegistros() {
        if(sinMachearSeleccionado == null || opcionMachearSeleccionado == null){
            tabIndex = 3;
            displayErrorMessageToUser("", "Debe seleccionar un establecimiento de salud de cada tabla ");
        }else{
        try{
            InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
            //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
            if(((Integer) selectOneMenuArchivo.getValue())==1){
                institucionesMacheadas.setInstitucion1(sinMachearSeleccionado);
                institucionesMacheadas.setInstitucion2(opcionMachearSeleccionado);
                printObjectivesToFile("LOG_OPERACIONES.txt", "MATCHEO MANUAL;" + sinMachearSeleccionado.getCodigoPrincipal() + ";" + opcionMachearSeleccionado.getCodigoPrincipal()+";");
            }else{
                institucionesMacheadas.setInstitucion1(opcionMachearSeleccionado);
                institucionesMacheadas.setInstitucion2(sinMachearSeleccionado);
                printObjectivesToFile("LOG_OPERACIONES.txt", "MATCHEO MANUAL;" + opcionMachearSeleccionado.getCodigoPrincipal() + ";" + sinMachearSeleccionado.getCodigoPrincipal()+";");
            }
            institucionesMacheadas.setEstado("CONFIRMADO");
            institucionesMacheadas.setModo("USUARIO");
            getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
            sinMachearSeleccionado = null;
            opcionMachearSeleccionado = null;
            tabIndex = 3;
            displayInfoMessageToUser("Exito", "Los establecimientos se han vinculado exitosamente");
            codigoEstablecimientoFiltroTabla4.setValue(null);
            tipoServicioFiltroTabla4.setValue(null);
            distritoFiltroTabla4.setValue(null);
            nombreEstablecimientoFiltroTabla4.setValue(null);
    
            codigoEstablecimientoFiltroTabla5.setValue(null);
            tipoServicioFiltroTabla5.setValue(null);
            distritoFiltroTabla5.setValue(null);
            nombreEstablecimientoFiltroTabla5.setValue(null);
        } catch (ConstraintViolationException cve) {
            Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
            System.out.println("*** Violación de restricciones de la base de datos: " + violations);
        } catch (Exception e) {
            System.out.println("Problema para guardar macheo " + e.getMessage() + " "
            + e.getLocalizedMessage()
            + sinMachearSeleccionado.getNombreInstitucionOriginal() + "-" + opcionMachearSeleccionado.getNombreInstitucionOriginal());
        }}
    }
    
    public void seleccionarOpcion(SelectEvent event) {
        opcionMachearSeleccionado = ((InstitucionTemp) event.getObject());
    }
    
    public void seleccionarSinMachear(SelectEvent event) {
        sinMachearSeleccionado = ((InstitucionTemp) event.getObject());
    }
    
    // <editor-fold defaultstate="collapsed" desc="Método onclick para desconfirmar un registro confirmado">
    public void deleteConfirmadoOnClick(InstitucionesMacheadas institucionesMacheadas) {
        filaConfirmadaSeleccionada = institucionesMacheadas;
        RequestContext rcontext = RequestContext.getCurrentInstance();
        rcontext.execute("confirmadosDeleteDialogWidget.show()");
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Método onclick para des-rechazar un registro rechazado">
    public void deleteRechazadoOnClick(InstitucionesMacheadas institucionesMacheadas) {
        filaRechazadaSeleccionada = institucionesMacheadas;
        RequestContext rcontext = RequestContext.getCurrentInstance();
        rcontext.execute("rechazadosDeleteDialogWidget.show()");
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Método principal del proceso de matching, se cargan los archivos y se verifica el matcheo para cada par posible">
    public void registrarDatos() {

        Boolean error = false;
        //Validacion de que se hayan adjuntado los dos archivos
        if ((urlPrimerArchivo == null || urlPrimerArchivo.compareTo("") == 0)
            || (urlSegundoArchivo == null || urlSegundoArchivo.compareTo("") == 0)) {
            error = true;
            displayErrorMessageToUser("Debe adjuntar ambos archivos", "");
        }

        if (error == false) {
            
            try {
                long cantRegistros = 0;

                //Comprobacion de si la region ya fue macheada para no volver a machear
                Query q = em.createQuery("SELECT count(im) FROM InstitucionesMacheadas im "
                    + "where im.institucion1.nombreDepartamento like :nombreDepartamento "
                    + "AND im.institucion2.nombreDepartamento like :nombreDepartamento");
                q.setParameter("nombreDepartamento", (String) selectOneMenuDepartamento.getValue());
                cantRegistros = (Long) q.getSingleResult();
            
                //Si la region no fue macheada todavia, se machea
                if(cantRegistros == 0){

                    //Se guardan los establecimientos de los dos archivos de entrada
                    //en la tabla temporal institucion_temp, se diferencian en el campo tipo
                    //tipo=1 para el archivo 1
                    //tipo=2 para el archivo 2
                    guardarDatosTablaTemporal();

                    //Se leen los establecimientos del archivo 1 desde la tabla temporal institucion_temp
                    //del departamento seleccionado para el matching
                    List<InstitucionTemp> institucionTempList;
                    q = em.createQuery("SELECT i FROM InstitucionTemp i where i.tipo = 1 "
                            + "AND i.nombreDepartamento like :nombreDepartamento ");
                    q.setParameter("nombreDepartamento", (String) selectOneMenuDepartamento.getValue());
                    institucionTempList = q.getResultList();
                    contadorMacheador = 0;

                    System.out.println("******************************************************* EMPIEZA EL MACHEO " + new Date());
                    
                    for (InstitucionTemp item : institucionTempList) {

                        //Se leen los establecimientos del archivo 2 desde la tabla temporal institucion_temp
                        //del departamento seleccionado para el matching
                        q = em.createQuery("SELECT i FROM InstitucionTemp i WHERE i.tipo = 2 "
                                + "AND i.nombreDepartamento LIKE :nombreDepartamento ");
                        q.setParameter("nombreDepartamento", (String) selectOneMenuDepartamento.getValue());
                        List<InstitucionTemp> institucion2List = q.getResultList();
                        //Para cada par posible entre los establecimientos del archivo 1 y archivo 2, 
                        //se verifica si existe un matching
                        for (InstitucionTemp institucion2 : institucion2List) {
                            verificarMacheo(item, institucion2);
                        }
                    }
                    System.out.println("******************************************************* TERMINA EL MACHEO " + new Date());

                }
            } catch (Exception e) {
                    System.out.println("Error en el proceso de macheo: " + e.getMessage());
            }
        }
            
        tabIndex = 0;
        archivo = 1;
        limpiarFiltros();
    }
    // </editor-fold>
    
    public boolean comprobarTokensNumericos(String nombre1, String nombre2){
        Boolean match = false;
        StringTokenizer tokensNombre1 = new StringTokenizer(nombre1, " ");
        StringTokenizer tokensNombre2 = new StringTokenizer(nombre2, " ");
        List<Integer> numerosNombre1 = new ArrayList();
        List<Integer> numerosNombre2 = new ArrayList();
        //Iterar los tokens del nombre1
        while (tokensNombre1.hasMoreTokens()) {
            String str = tokensNombre1.nextToken();
            try {
                Integer.parseInt(str);
                numerosNombre1.add(Integer.parseInt(str));
            } catch (Exception e) {
            }
        }
        //Iterar los tokens del nombre2
        while (tokensNombre2.hasMoreTokens()) {
            String str = tokensNombre2.nextToken();
            try {
                Integer.parseInt(str);
                numerosNombre2.add(Integer.parseInt(str));
            } catch (Exception e) {
            }
        }
        if(numerosNombre1.isEmpty() || numerosNombre2.isEmpty()){
            return true;
        }
       
        int cantCoincidir = 0;
        if(numerosNombre1.size() <= numerosNombre2.size()){
            cantCoincidir = numerosNombre1.size();
        }else{
            cantCoincidir = numerosNombre2.size();
        }
        int cantActual = 0;
        for(int i=0; i<numerosNombre1.size(); i++){
            if(numerosNombre2.contains(numerosNombre1.get(i))){
                cantActual++;
            }
        }
        if(cantActual == cantCoincidir){
            match = true;
        }
        return match;
    }
    
    public static int calculateLevensteinDistance(String s1, String s2) {
		int distance = StringUtils.getLevenshteinDistance(s1, s2);
		double ratio = ((double) distance) / (Math.max(s1.length(), s2.length()));
		return 100 - new Double(ratio*100).intValue();		
	}

    public void verificarMacheo(InstitucionTemp institucion1, InstitucionTemp institucion2) {
        
        try {
            //contadorMacheador = 0;
            //System.out.println("**************");
            String nombre1 = normalizarNombre(institucion1.getNombreInstitucion());
            String nombre2 = normalizarNombre(institucion2.getNombreInstitucion());
            //String nombre1 = institucion1.getNombreInstitucion();
            //String nombre2 = institucion2.getNombreInstitucion();
            if(nombre1.contains("SANTA") && nombre2.contains("SANTA")){
                nombre1 = nombre1.replace("SANTA", "");
                nombre2 = nombre2.replace("SANTA", "");
            }
            if(nombre1.contains("SAN ") && nombre2.contains("SAN ")){
                nombre1 = nombre1.replace("SAN ", " ");
                nombre2 = nombre2.replace("SAN ", " ");
            }
            if(nombre1.contains("GENERAL") && nombre2.contains("GENERAL")){
                nombre1 = nombre1.replace("GENERAL", "");
                nombre2 = nombre2.replace("GENERAL", "");
            }
            if(nombre1.contains("HR") && nombre2.contains("HR")){
                nombre1 = nombre1.replace("HR", "");
                nombre2 = nombre2.replace("HR", "");
            }
            if(nombre1.contains("HR") && nombre2.contains("HOSPITAL REGIONAL")){
                nombre1 = nombre1.replace("HR", "");
                nombre2 = nombre2.replace("HOSPITAL REGIONAL", "");
            }
            if(nombre1.contains("HOSPITAL REGIONAL") && nombre2.contains("HR")){
                nombre1 = nombre1.replace("HOSPITAL REGIONAL", "");
                nombre2 = nombre2.replace("HR", "");
            }
            if(nombre1.contains("HD") && nombre2.contains("HD")){
                nombre1 = nombre1.replace("HD", "");
                nombre2 = nombre2.replace("HD", "");
            }
            if(nombre1.contains("HD") && nombre2.contains("HOSPITAL DISTRITAL")){
                nombre1 = nombre1.replace("HD", "");
                nombre2 = nombre2.replace("HOSPITAL DISTRITAL", "");
            }
            if(nombre1.contains("HOSPITAL DISTRITAL") && nombre2.contains("HD")){
                nombre1 = nombre1.replace("HOSPITAL DISTRITAL", "");
                nombre2 = nombre2.replace("HD", "");
            }
            int coeficienteSimilaridad = calculateLevensteinDistance(nombre1, nombre2);
            //System.out.println("**************");
            //System.out.println("nombre1: " + institucion1.getNombreInstitucion());
            //System.out.println("nombre2: " + institucion2.getNombreInstitucion());
            //System.out.println("coeficiente: " + coeficienteSimilaridad);
            //System.out.println("**************");

                if(coeficienteSimilaridad >= 75 && coeficienteSimilaridad < 101){
                    Boolean agregar = false;
                    if(comprobarTokensNumericos(nombre1,nombre2)){
                        agregar= true;
                        if(nombre1.matches(".+ITA$") && !nombre2.matches(".+ITA$")){
                            agregar = false;
                        }else if(nombre2.matches(".+ITA$") && !nombre1.matches(".+ITA$")){
                            agregar = false;
                        }
                    }
                    if(agregar == true){
                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                        institucionesMacheadas.setInstitucion1(institucion1);
                        institucionesMacheadas.setInstitucion2(institucion2);
                        institucionesMacheadas.setEstado("SIN ESTADO");
                        institucionesMacheadas.setModo("AUTOMATICO");
                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                        printObjectivesToFile("LOG_OPERACIONES.txt", "MATCHEO AUTOMATICO;" + institucion1.getCodigoPrincipal() + ";" + institucion2.getCodigoPrincipal()+";");
                    }
                }else{
                    int cantTokensCoincidentes = 0;
                    StringTokenizer tokensNombre1 = new StringTokenizer(nombre1, " ");
                    StringTokenizer tokensNombre2 = new StringTokenizer(nombre2, " ");
                    int cantTotal = tokensNombre1.countTokens() + tokensNombre2.countTokens();
                    //Iterar los tokens del nombre1
                    while (tokensNombre1.hasMoreTokens()) {
                        String str = tokensNombre1.nextToken();
                        tokensNombre2 = new StringTokenizer(nombre2, " ");
                        while (tokensNombre2.hasMoreTokens()) {
                            String str2 = tokensNombre2.nextToken();
                            if(str.compareTo(str2)==0){
                                cantTokensCoincidentes++;
                                break;
                            }
                        }
                    }
                    tokensNombre2 = new StringTokenizer(nombre2, " ");
                    //Iterar los tokens del nombre2
                    while (tokensNombre2.hasMoreTokens()) {
                        String str2 = tokensNombre2.nextToken();
                        tokensNombre1 = new StringTokenizer(nombre1, " ");
                        while (tokensNombre1.hasMoreTokens()) {
                            String str = tokensNombre1.nextToken();
                            if(str2.compareTo(str)==0){
                                cantTokensCoincidentes++;
                                break;
                            }
                        }
                    }
                    double similaridadTokens = 0;
                    if(cantTotal > 0){
                        similaridadTokens = (cantTokensCoincidentes*100)/cantTotal;
                    }
                    if(similaridadTokens > 60){
                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                        institucionesMacheadas.setInstitucion1(institucion1);
                        institucionesMacheadas.setInstitucion2(institucion2);
                        institucionesMacheadas.setEstado("SIN ESTADO");
                        institucionesMacheadas.setModo("AUTOMATICO");
                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                        printObjectivesToFile("LOG_OPERACIONES.txt", "MATCHEO AUTOMATICO;" + institucion1.getCodigoPrincipal() + ";" + institucion2.getCodigoPrincipal()+";");
                    }
                            
                }
            /*System.out.println("**************");
            String nombre1 = normalizarNombre(institucion1.getNombreIntitucion());
            String nombre2 = normalizarNombre(institucion2.getNombreIntitucion());
            if(nombre1.contains("SANTA") && nombre2.contains("SANTA")){
                nombre1 = nombre1.replace("SANTA", "");
                nombre2 = nombre2.replace("SANTA", "");
            }else if(nombre1.contains("SAN ") && nombre2.contains("SAN ")){
                nombre1 = nombre1.replace("SAN ", " ");
                nombre2 = nombre2.replace("SAN ", " ");
            }else if(nombre1.contains("GENERAL") && nombre2.contains("GENERAL")){
                nombre1 = nombre1.replace("GENERAL", "");
                nombre2 = nombre2.replace("GENERAL", "");
            }
                int coeficienteSimilaridad = StringMatching.calcularDistanciaDamerauLevenshtein(nombre1, nombre2);
                System.out.println("**************");
                System.out.println("nombre1: " + institucion1.getNombreIntitucion());
                System.out.println("nombre2: " + institucion2.getNombreIntitucion());
                System.out.println("coeficiente: " + calculateLevensteinDistance(institucion1.getNombreIntitucion(),
                        institucion2.getNombreIntitucion()));
                System.out.println("**************");
                int longitud;
                if (institucion2.getNombreIntitucion().length() > institucion1.getNombreIntitucion().length()) {
                    longitud = institucion2.getNombreIntitucion().length();
                } else {
                    longitud = institucion1.getNombreIntitucion().length();
                }
                if(coeficienteSimilaridad <= (longitud*25)/100){
                    Boolean agregar = false;
                    if(comprobarTokensNumericos(nombre1,nombre2)){
                        agregar= true;
                    }
                    if(agregar == true){
                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                        institucionesMacheadas.setInstitucion1(institucion1);
                        institucionesMacheadas.setInstitucion2(institucion2);
                        institucionesMacheadas.setEstado(true);
                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                    }
                }
/*
            // <editor-fold defaultstate="collapsed" desc="ESCUELAS">
            if (((institucion1.getNombreIntitucion()).contains("ESCUELA")
                    && (institucion2.getNombreIntitucion()).contains("ESCUELA"))
                    || ((institucion1.getNombreIntitucion()).contains("ESCUELA")
                    && (!(institucion2.getNombreIntitucion()).contains("COLEGIO")))
                    || ((institucion1.getNombreIntitucion()).contains("ESCUELA")
                    && (!(institucion2.getNombreIntitucion()).contains("LICEO")))
                    || ((institucion1.getNombreIntitucion()).contains("ESCUELA")
                    && (!(institucion2.getNombreIntitucion()).contains("CENTRO")))
                    || ((institucion1.getNombreIntitucion()).contains("ESCUELA")
                    && (!(institucion2.getNombreIntitucion()).contains("SEDE")))
                    || ((institucion1.getNombreIntitucion()).contains("CENTRO")
                    && ((institucion2.getNombreIntitucion()).contains("CENTRO")))) {

                String nombre1 = normalizarNombre(institucion1.getNombreIntitucion());
                String nombre2 = normalizarNombre(institucion2.getNombreIntitucion());
                int coeficienteSimilaridad = StringMatching.calcularDistanciaDamerauLevenshtein(nombre1, nombre2);
                int longitud;
                if (institucion2.getNombreIntitucion().length() > institucion1.getNombreIntitucion().length()) {
                    longitud = institucion2.getNombreIntitucion().length();
                } else {
                    longitud = institucion1.getNombreIntitucion().length();
                }
                //if(coeficienteSimilaridad <= (longitud*90)/100){
                StringTokenizer tokens = new StringTokenizer(institucion1.getNombreIntitucion(), " ");
                //int cant_tokens = tokens.countTokens();
                //int cant_tokens_coincidentes = 0;
                int i = 0;
                List<Integer> numerosEscuelas = new ArrayList();
                List<String> meses = new ArrayList();
                meses.add("ENERO");
                meses.add("FEBRERO");
                meses.add("MARZO");
                meses.add("ABRIL");
                meses.add("MAYO");
                meses.add("JUNIO");
                meses.add("JULIO");
                meses.add("AGOSTO");
                meses.add("SETIEMBRE");
                meses.add("OCTUBRE");
                meses.add("NOVIEMBRE");
                meses.add("DICIEMBRE");
                Boolean tieneMeses1 = false;
                while (tokens.hasMoreTokens()) {
                    String str = tokens.nextToken();
                    if (meses.contains(str)) {
                        tieneMeses1 = true;
                    }
                    try {
                        Integer.parseInt(str);
                        numerosEscuelas.add(Integer.parseInt(str));
                    } catch (Exception e) {
                    }
                }
                tokens = new StringTokenizer(institucion2.getNombreIntitucion(), " ");
                while (tokens.hasMoreTokens()) {
                    String str = tokens.nextToken();
                    try {
                        Integer.parseInt(str);
                        if (numerosEscuelas.contains(Integer.parseInt(str))) {
                            i++;
                        }
                    } catch (Exception e) {
                    }
                }
                int minimo = 0;
                if (tieneMeses1 == true) {
                    minimo = 1;
                }
                if (i > minimo) {
                    //ystem.out.println("entraaa");
                    contadorMacheador++;
                    InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                    //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                    institucionesMacheadas.setInstitucion1(institucion1);
                    institucionesMacheadas.setInstitucion2(institucion2);
                    institucionesMacheadas.setEstado(true);
                    getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                }
                //}
            }
            // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="LICEOS">
            if (
                    ((institucion1.getNombreIntitucion()).contains("LICEO")
                    && (institucion2.getNombreIntitucion()).contains("LICEO")) ||
                    ((institucion1.getNombreIntitucion()).contains("LICEO")
                    && !(institucion2.getNombreIntitucion()).contains("ESCUELA")) ||
                    ((institucion1.getNombreIntitucion()).contains("LICEO")
                    && !(institucion2.getNombreIntitucion()).contains("CENTRO")) ||
                    ((institucion1.getNombreIntitucion()).contains("LICEO")
                    && !(institucion2.getNombreIntitucion()).contains("SEDE"))) {

                Boolean macheado = false;
                String nombre1Normalizado = (normalizarNombre(institucion1.getNombreIntitucion())).trim();
                String nombre2Normalizado = (normalizarNombre(institucion2.getNombreIntitucion())).trim();
                if(nombre1Normalizado.compareTo(nombre2Normalizado)==0){
                    contadorMacheador++;
                    InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                    //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                    institucionesMacheadas.setInstitucion1(institucion1);
                    institucionesMacheadas.setInstitucion2(institucion2);
                    institucionesMacheadas.setEstado(true);
                    getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                    macheado=true;
                }
            }
            // </editor-fold>

            // <editor-fold defaultstate="collapsed" desc="COLEGIOS">
            if (
                    ((institucion1.getNombreIntitucion()).contains("COLEGIO")
                    && (institucion2.getNombreIntitucion()).contains("COLEGIO")) ||
                    ((institucion1.getNombreIntitucion()).contains("COLEGIO")
                    && !(institucion2.getNombreIntitucion()).contains("ESCUELA")) ||
                    ((institucion1.getNombreIntitucion()).contains("COLEGIO")
                    && !(institucion2.getNombreIntitucion()).contains("CENTRO")) ||
                    ((institucion1.getNombreIntitucion()).contains("COLEGIO")
                    && !(institucion2.getNombreIntitucion()).contains("SEDE"))) {

                Boolean macheado = false;
                String nombre1Normalizado = (normalizarNombre(institucion1.getNombreIntitucion())).trim();
                String nombre2Normalizado = (normalizarNombre(institucion2.getNombreIntitucion())).trim();
                if(nombre1Normalizado.compareTo(nombre2Normalizado)==0){
                    contadorMacheador++;
                    InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                    //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                    institucionesMacheadas.setInstitucion1(institucion1);
                    institucionesMacheadas.setInstitucion2(institucion2);
                    institucionesMacheadas.setEstado(true);
                    getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                    macheado=true;
                }
                if(macheado == false){
                //tokens de institucion 1
                StringTokenizer tokens = new StringTokenizer(institucion1.getNombreIntitucion(), " ");
                int cant_tokens = tokens.countTokens();
                int cant_tokens_coincidentes = 0;
                int i = 0;
                List<Integer> numeros1 = new ArrayList();
                while (tokens.hasMoreTokens()) {
                    String str = tokens.nextToken();
                    try {
                        Integer.parseInt(str);
                        numeros1.add(Integer.parseInt(str));
                    } catch (Exception e) {
                    }
                }
                //tokens de institucion 2
                List<Integer> numeros2 = new ArrayList();
                tokens = new StringTokenizer(institucion2.getNombreIntitucion(), " ");
                while (tokens.hasMoreTokens()) {
                    String str = tokens.nextToken();
                    try {
                        Integer.parseInt(str);
                        numeros2.add(Integer.parseInt(str));
                    } catch (Exception e) {
                    }
                }

                if (numeros1.isEmpty() && numeros2.isEmpty()) {
                    if ((institucion2.getNombreIntitucion()).compareTo(institucion1.getNombreIntitucion()) == 0) {
                        contadorMacheador++;
                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                        institucionesMacheadas.setInstitucion1(institucion1);
                        institucionesMacheadas.setInstitucion2(institucion2);
                        institucionesMacheadas.setEstado(true);
                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                    } else {

                        String nombre1 = normalizarNombre(institucion1.getNombreIntitucion());
                        String nombre2 = normalizarNombre(institucion2.getNombreIntitucion());
                        int coeficienteSimilaridad = StringMatching.calcularDistanciaDamerauLevenshtein(nombre1, nombre2);
                        int longitud;
                        if (nombre2.length() > nombre1.length()) {
                            longitud = nombre2.length();
                        } else {
                            longitud = nombre1.length();
                        }
                        //System.out.println("nombre1 " + nombre1);
                        //System.out.println("nombre2 " + nombre2);
                        //System.out.println("coeficienteSimilaridad " + coeficienteSimilaridad);
                        //System.out.println("longitud " + longitud);
                        //System.out.println("lllllllllllllllllllllllllll " + ((longitud * 20) / 100));
                        if (coeficienteSimilaridad <= (longitud * 30) / 100) {
                            if (institucion1.getNombreIntitucion().contains("NACIONAL") && institucion2.getNombreIntitucion().contains("PRIVADO")) {
                            } else if (institucion2.getNombreIntitucion().contains("NACIONAL") && institucion1.getNombreIntitucion().contains("PRIVADO")) {
                            } // <editor-fold defaultstate="collapsed" desc="Verificar si es CENTRO">
                            /*else if ((institucion1.getNombreIntitucion()).contains("CENTRO") == true) {
                                Pattern regPatt = Pattern.compile("\\s.*-.*\\s");
                                Matcher regMatch = regPatt.matcher(institucion1.getNombreIntitucion());
                                String patron = null;
                                while (regMatch.find()) {
                                    patron = regMatch.group();
                                    break;
                                }
                                if (patron != null) {
                                    if (institucion2.getNombreIntitucion().contains(patron)) {
                                        contadorMacheador++;
                                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                                        institucionesMacheadas.setInstitucion1(institucion1);
                                        institucionesMacheadas.setInstitucion2(institucion2);
                                        institucionesMacheadas.setEstado(true);
                                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                                    }
                                }
                            } // </editor-fold>
                            // <editor-fold defaultstate="collapsed" desc="Verificar no machear LICEO con COLEGIO">
                            else if (institucion1.getNombreIntitucion().contains("LICEO") && institucion2.getNombreIntitucion().contains("COLEGIO")) {
                            } else if (institucion1.getNombreIntitucion().contains("COLEGIO") && institucion2.getNombreIntitucion().contains("LICEO")) {
                            } // </editor-fold>
                            // <editor-fold defaultstate="collapsed" desc="Verificar nombres de santos">
                            else if (institucion1.getNombreIntitucion().contains("SAN ")) {
                                tokens = new StringTokenizer(institucion1.getNombreIntitucion(), " ");
                                i = 0;
                                boolean bandera = false;
                                String str = null;
                                while (tokens.hasMoreTokens()) {
                                    str = tokens.nextToken();
                                    if (bandera == true) {
                                        break;
                                    }
                                    if (str.compareTo("SAN") == 0) {
                                        bandera = true;
                                    }
                                    i++;
                                }
                                if (str != null) {
                                    if (institucion2.getNombreIntitucion().contains(str)) {
                                        contadorMacheador++;
                                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                                        institucionesMacheadas.setInstitucion1(institucion1);
                                        institucionesMacheadas.setInstitucion2(institucion2);
                                        institucionesMacheadas.setEstado(true);
                                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                                    }
                                }
                            } // </editor-fold>
                            else if (institucion1.getNombreIntitucion().contains("SANTA ")) {
                                tokens = new StringTokenizer(institucion1.getNombreIntitucion(), " ");
                                i = 0;
                                boolean bandera = false;
                                String str = null;
                                while (tokens.hasMoreTokens()) {
                                    str = tokens.nextToken();
                                    if (bandera == true) {
                                        break;
                                    }
                                    if (str.compareTo("SANTA") == 0) {
                                        bandera = true;
                                    }
                                    i++;
                                }
                                if (str != null) {
                                    if (institucion2.getNombreIntitucion().contains(str)) {
                                        contadorMacheador++;
                                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                                        institucionesMacheadas.setInstitucion1(institucion1);
                                        institucionesMacheadas.setInstitucion2(institucion2);
                                        institucionesMacheadas.setEstado(true);
                                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                                    }
                                }
                            } else if (institucion1.getNombreIntitucion().contains("SANTO ")) {
                                tokens = new StringTokenizer(institucion1.getNombreIntitucion(), " ");
                                i = 0;
                                boolean bandera = false;
                                String str = null;
                                while (tokens.hasMoreTokens()) {
                                    str = tokens.nextToken();
                                    if (bandera == true) {
                                        break;
                                    }
                                    if (str.compareTo("SANTO") == 0) {
                                        bandera = true;
                                    }
                                    i++;
                                }
                                if (str != null) {
                                    if (institucion2.getNombreIntitucion().contains(str)) {
                                        contadorMacheador++;
                                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                                        institucionesMacheadas.setInstitucion1(institucion1);
                                        institucionesMacheadas.setInstitucion2(institucion2);
                                        institucionesMacheadas.setEstado(true);
                                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                                    }
                                }
                            } else {
                                tokens = new StringTokenizer(institucion1.getNombreIntitucion(), " ");
                                cant_tokens = tokens.countTokens();
                                cant_tokens_coincidentes = 0;
                                i = 0;
                                while (tokens.hasMoreTokens()) {
                                    String str = tokens.nextToken();
                                    if (institucion2.getNombreIntitucion().contains(str)) {
                                        cant_tokens_coincidentes++;
                                    }
                                    i++;
                                }
                                //System.out.println("**** " + (double) cant_tokens_coincidentes / (double) cant_tokens);
                                if ((double) cant_tokens_coincidentes / (double) cant_tokens > 0.5) {
                                    contadorMacheador++;
                                    InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                                    //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                                    institucionesMacheadas.setInstitucion1(institucion1);
                                    institucionesMacheadas.setInstitucion2(institucion2);
                                    institucionesMacheadas.setEstado(true);
                                    getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                                }
                            }
                        } else {
                            tokens = new StringTokenizer(nombre1, " ");
                            cant_tokens = tokens.countTokens();
                            cant_tokens_coincidentes = 0;
                            i = 0;
                            while (tokens.hasMoreTokens()) {
                                String str = tokens.nextToken();
                                if (nombre2.contains(str)) {
                                    cant_tokens_coincidentes++;
                                }
                                i++;
                            }
                            //System.out.println("**** " + (double) cant_tokens_coincidentes / (double) cant_tokens);
                            if ((double) cant_tokens_coincidentes / (double) cant_tokens > 0.9) {
                                contadorMacheador++;
                                InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                                //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                                institucionesMacheadas.setInstitucion1(institucion1);
                                institucionesMacheadas.setInstitucion2(institucion2);
                                institucionesMacheadas.setEstado(true);
                                getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                            }
                        }
                    }
                }}
            } // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="CENTROS">
            else if (((institucion1.getNombreIntitucion()).contains("CENTRO")
                    && (!(institucion2.getNombreIntitucion()).contains("CENTRO")))
                    || ((institucion1.getNombreIntitucion()).contains("CENTRO")
                    && (!(institucion2.getNombreIntitucion()).contains("ESCUELA")))
                    || ((institucion1.getNombreIntitucion()).contains("CENTRO")
                    && (!(institucion2.getNombreIntitucion()).contains("COLEGIO")))
                    || ((institucion1.getNombreIntitucion()).contains("CENTRO")
                    && (!(institucion2.getNombreIntitucion()).contains("LICEO")))
                    || ((institucion1.getNombreIntitucion()).contains("CENTRO")
                    && (!(institucion2.getNombreIntitucion()).contains("SEDE")))) {
                
                Boolean macheado = false;
                
                Pattern regPatt = Pattern.compile("\\w+-[0-9]+");
                Matcher regMatch = regPatt.matcher(institucion1.getNombreIntitucion());
                List<String> tokenList = new ArrayList<String>();
                while (regMatch.find()) {
                    tokenList.add(regMatch.group());
                    break;
                }
                
                Pattern regPatt2 = Pattern.compile("\\w+-[0-9]+");
                Matcher regMatch2 = regPatt2.matcher(institucion2.getNombreIntitucion());
                List<String> tokenList2 = new ArrayList<String>();
                while (regMatch2.find()) {
                    tokenList2.add(regMatch2.group());
                }
                
                Boolean machea = false;
                for(String item : tokenList){
                    if(tokenList2.contains(item)){
                        machea = true;
                        break;
                    }
                }
                    
                if (machea == true) {
                    //if (institucion2.getNombreIntitucion().contains(patron.trim() + " ")) {
                        contadorMacheador++;
                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                        institucionesMacheadas.setInstitucion1(institucion1);
                        institucionesMacheadas.setInstitucion2(institucion2);
                        institucionesMacheadas.setEstado(true);
                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                        macheado = true;
                    //}
                }
                
            }
            // </editor-fold>
            
            // <editor-fold defaultstate="collapsed" desc="SEDE">
            else if (((institucion1.getNombreIntitucion()).contains("SEDE")
                    && (!(institucion2.getNombreIntitucion()).contains("SEDE")))
                    || ((institucion1.getNombreIntitucion()).contains("SEDE")
                    && (!(institucion2.getNombreIntitucion()).contains("ESCUELA")))
                    || ((institucion1.getNombreIntitucion()).contains("SEDE")
                    && (!(institucion2.getNombreIntitucion()).contains("COLEGIO")))
                    || ((institucion1.getNombreIntitucion()).contains("SEDE")
                    && (!(institucion2.getNombreIntitucion()).contains("LICEO"))) ) {
                
                Boolean macheado = false;
                
                Pattern regPatt = Pattern.compile("[0-9]+");
                Matcher regMatch = regPatt.matcher(institucion1.getNombreIntitucion());
                List<String> tokenList = new ArrayList<String>();
                while (regMatch.find()) {
                    tokenList.add(regMatch.group());
                    break;
                }
                
                Pattern regPatt2 = Pattern.compile("[0-9]+");
                Matcher regMatch2 = regPatt2.matcher(institucion2.getNombreIntitucion());
                List<String> tokenList2 = new ArrayList<String>();
                while (regMatch2.find()) {
                    tokenList2.add(regMatch2.group().trim());
                }
             
                Boolean machea = false;
                if(tokenList.size() == 1 && tokenList2.size()==1){
                    if((tokenList.get(0)).compareTo(tokenList2.get(0))==0){
                        machea=true;
                    }
                }
                    
                if (machea == true) {
                    //if (institucion2.getNombreIntitucion().contains(patron.trim() + " ")) {
                        contadorMacheador++;
                        InstitucionesMacheadas institucionesMacheadas = new InstitucionesMacheadas();
                        //institucionesMacheadas.setIdInstitucionesMacheadas(contadorMacheador);
                        institucionesMacheadas.setInstitucion1(institucion1);
                        institucionesMacheadas.setInstitucion2(institucion2);
                        institucionesMacheadas.setEstado(true);
                        getInstitucionesMacheadasFacade().createInstitucionesMacheadas(institucionesMacheadas);
                        macheado = true;
                    //}
                }
                
            }
            // </editor-fold>
*/
        } catch (ConstraintViolationException cve) {
                        Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
                        System.out.println("*** Violación de restricciones de la base de datos: " + violations);
                    } 
        catch (Exception e) {
            System.out.println("Problema para guardar macheo " + e.getMessage() + " "
                     + e.getLocalizedMessage()
                    + institucion1.getNombreInstitucionOriginal() + "-" + institucion2.getNombreInstitucionOriginal());
        }
    }

    public void valueChangeListener1() {
        if (((String) selectOneMenuNombre1.getValue()) != null) {
            columnasList2.remove((String) selectOneMenuNombre1.getValue());
            columnasList3.remove((String) selectOneMenuNombre1.getValue());
            columnasList4.remove((String) selectOneMenuNombre1.getValue());
            columnasList5.remove((String) selectOneMenuNombre1.getValue());
        }
        if (nombre1 != null) {
            columnasList2.add(nombre1);
            columnasList3.add(nombre1);
            columnasList4.add(nombre1);
            columnasList5.add(nombre1);
        }
    }

    public void valueChangeListener2() {
        if (((String) selectOneMenuDepartamento1.getValue()) != null) {
            columnasList1.remove((String) selectOneMenuDepartamento1.getValue());
            columnasList3.remove((String) selectOneMenuDepartamento1.getValue());
            columnasList4.remove((String) selectOneMenuDepartamento1.getValue());
            columnasList5.remove((String) selectOneMenuDepartamento1.getValue());
        }
        if (departamento1 != null) {
            columnasList1.add(departamento1);
            columnasList3.add(departamento1);
            columnasList4.add(departamento1);
            columnasList5.add(departamento1);
        }
    }

    public void valueChangeListener3() {
        if (((String) selectOneMenuDistrito1.getValue()) != null) {
            columnasList1.remove((String) selectOneMenuDistrito1.getValue());
            columnasList2.remove((String) selectOneMenuDistrito1.getValue());
            columnasList4.remove((String) selectOneMenuDistrito1.getValue());
            columnasList5.remove((String) selectOneMenuDistrito1.getValue());
        }
        if (distrito1 != null) {
            columnasList1.add(distrito1);
            columnasList2.add(distrito1);
            columnasList4.add(distrito1);
            columnasList5.add(distrito1);
        }
    }

    public void valueChangeListener4() {
        if (((String) selectOneMenuCodigo1.getValue()) != null) {
            columnasList1.remove((String) selectOneMenuCodigo1.getValue());
            columnasList2.remove((String) selectOneMenuCodigo1.getValue());
            columnasList3.remove((String) selectOneMenuCodigo1.getValue());
            columnasList5.remove((String) selectOneMenuCodigo1.getValue());
        }
        if (codigo1 != null) {
            columnasList1.add(codigo1);
            columnasList2.add(codigo1);
            columnasList3.add(codigo1);
            columnasList5.add(codigo1);
        }
    }
    
    public void valueChangeListener9() {
        if (((String) selectOneMenuTipo1.getValue()) != null) {
            columnasList1.remove((String) selectOneMenuTipo1.getValue());
            columnasList2.remove((String) selectOneMenuTipo1.getValue());
            columnasList3.remove((String) selectOneMenuTipo1.getValue());
            columnasList4.remove((String) selectOneMenuTipo1.getValue());
        }
        if (tipo1 != null) {
            columnasList1.add(tipo1);
            columnasList2.add(tipo1);
            columnasList3.add(tipo1);
            columnasList4.add(tipo1);
        }
    }

    public void valueChangeListener5() {//1
        if (((String) selectOneMenuNombre2.getValue()) != null) {
            columnas2List2.remove((String) selectOneMenuNombre2.getValue());
            columnas2List3.remove((String) selectOneMenuNombre2.getValue());
            columnas2List4.remove((String) selectOneMenuNombre2.getValue());
            columnas2List5.remove((String) selectOneMenuNombre2.getValue());
        }
        if (nombre2 != null) {
            columnas2List2.add(nombre2);
            columnas2List3.add(nombre2);
            columnas2List4.add(nombre2);
            columnas2List5.add(nombre2);
        }
    }

    public void valueChangeListener6() {//2
        if (((String) selectOneMenuDepartamento2.getValue()) != null) {
            columnas2List1.remove((String) selectOneMenuDepartamento2.getValue());
            columnas2List3.remove((String) selectOneMenuDepartamento2.getValue());
            columnas2List4.remove((String) selectOneMenuDepartamento2.getValue());
            columnas2List5.remove((String) selectOneMenuDepartamento2.getValue());
        }
        if (departamento2 != null) {
            columnas2List1.add(departamento2);
            columnas2List3.add(departamento2);
            columnas2List4.add(departamento2);
            columnas2List5.add(departamento2);
        }
    }

    public void valueChangeListener7() {//3
        if (((String) selectOneMenuDistrito2.getValue()) != null) {
            columnas2List1.remove((String) selectOneMenuDistrito2.getValue());
            columnas2List2.remove((String) selectOneMenuDistrito2.getValue());
            columnas2List4.remove((String) selectOneMenuDistrito2.getValue());
            columnas2List5.remove((String) selectOneMenuDistrito2.getValue());
        }
        if (distrito2 != null) {
            columnas2List1.add(distrito2);
            columnas2List2.add(distrito2);
            columnas2List4.add(distrito2);
            columnas2List5.add(distrito2);
        }
    }

    public void valueChangeListener8() {//4
        if (((String) selectOneMenuCodigo2.getValue()) != null) {
            columnas2List1.remove((String) selectOneMenuCodigo2.getValue());
            columnas2List2.remove((String) selectOneMenuCodigo2.getValue());
            columnas2List3.remove((String) selectOneMenuCodigo2.getValue());
            columnas2List5.remove((String) selectOneMenuCodigo2.getValue());
        }
        if (codigo2 != null) {
            columnas2List1.add(codigo2);
            columnas2List2.add(codigo2);
            columnas2List3.add(codigo2);
            columnas2List5.add(codigo2);
        }
    }
    
    public void valueChangeListener10() {//5
        if (((String) selectOneMenuTipo2.getValue()) != null) {
            columnas2List1.remove((String) selectOneMenuTipo2.getValue());
            columnas2List2.remove((String) selectOneMenuTipo2.getValue());
            columnas2List3.remove((String) selectOneMenuTipo2.getValue());
            columnas2List4.remove((String) selectOneMenuTipo2.getValue());
        }
        if (tipo2 != null) {
            columnas2List1.add(tipo2);
            columnas2List2.add(tipo2);
            columnas2List3.add(tipo2);
            columnas2List4.add(tipo2);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Guardar establecimientos desde archivos a tabla temporal institucion_temp">
    public void guardarDatosTablaTemporal() {
                
        try {

            //Eliminar todas las filas de la tabla temporal
            //getInstitucionesMacheadasFacade().deleteAllInstitucionesMacheadas();

            //Eliminar todas las filas de la tabla temporal
            //getInstitucionTempFacade().deleteAllInstitucionTemp();
            
            System.out.println("******************************Empieza insituciones 1: " + new Date());
            
            //Requiere que saa sea el primer archivo y siciap el segundo archivo
            
            //Si el departamento seleccionado para el matching es "PARQUES VIRTUALES", se guardan todos los
            //establecimientos del archivo 1, se setea el nombre del departamento a "PARQUES VIRTUALES" y la 
            //información del departamento se guarda junto a la información del distrito
            //Si el departamento seleccionado es cualquiera menos "PARQUES VIRTUALES", se guardan todos 
            //los establecimientos solamente del departamento seleccionado
            // <editor-fold defaultstate="collapsed" desc="Lectura del primer archivo">
            
            BufferedReader br = null;
            String line = "";
            String cvsSplitBy = ",";
            br = new BufferedReader(new FileReader(destinationFilePrimerArchivo));
            Integer count = 0;
            
            while ((line = br.readLine()) != null) {
                
                if (count > 0) { //para ignorar el titulo
                      
                    //Si el departamento seleccionado para machear es cualquiera menos "PARQUE REGIONAL VIRTUAL HOSPITALES ESPECIALIZADOS"
                    if(((String) selectOneMenuDepartamento.getValue()).compareTo("PARQUE REGIONAL VIRTUAL HOSPITALES ESPECIALIZADOS")!=0){
                        String[] resultado = line.split(cvsSplitBy);
                        String departamentoFila = normalizarNombreDepartamento(resultado[columnas1Map.get((String) selectOneMenuDepartamento1.getValue())]);
                        //Si el departamento de la fila que se esta leyendo del archivo coincide con el departamento seleccionado 
                        //para machear, se guarda el establecimiento
                        if(departamentoFila.compareTo((String) selectOneMenuDepartamento.getValue())==0){
                            InstitucionTemp institucionTemp = new InstitucionTemp();         
                            institucionTemp.setNombreInstitucion(normalizarNombreEntidad(resultado[columnas1Map.get((String) selectOneMenuNombre1.getValue())]));
                            institucionTemp.setNombreInstitucionOriginal(resultado[columnas1Map.get((String) selectOneMenuNombre1.getValue())]);
                            institucionTemp.setNombreDepartamento(normalizarNombreDepartamento(resultado[columnas1Map.get((String) selectOneMenuDepartamento1.getValue())]));
                            if (columnas1Map.get((String) selectOneMenuDistrito1.getValue()) != null) {
                                institucionTemp.setNombreDistrito(normalizarString(normalizarString(resultado[columnas1Map.get((String) selectOneMenuDistrito1.getValue())])));
                            }
                            if (columnas1Map.get((String) selectOneMenuCodigo1.getValue()) != null) {
                                institucionTemp.setCodigoPrincipal(normalizarString(normalizarString(resultado[columnas1Map.get((String) selectOneMenuCodigo1.getValue())])));
                            }
                            if (columnas1Map.get((String) selectOneMenuTipo1.getValue()) != null) {
                                institucionTemp.setCodigoSecundario(normalizarString(normalizarString(resultado[columnas1Map.get((String) selectOneMenuTipo1.getValue())])));
                            }
                            institucionTemp.setTipo(1);
                            try {
                                getInstitucionTempFacade().createInstitucionTemp(institucionTemp);
                            }catch (ConstraintViolationException cve) {
                                Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
                                System.out.println("*** Violación de restricciones de la base de datos: " + violations);
                            }catch(Exception e){
                                System.out.println("*** Problema para guardar establecimiento a partir de archivo 1: " + e.getMessage() + " " +
                                        e.getLocalizedMessage());
                            }
                        }
                    }else{
                        String[] resultado = line.split(cvsSplitBy);
                        InstitucionTemp institucionTemp = new InstitucionTemp();         
                        institucionTemp.setNombreInstitucion(normalizarNombreEntidad(resultado[columnas1Map.get((String) selectOneMenuNombre1.getValue())]));
                        institucionTemp.setNombreInstitucionOriginal(resultado[columnas1Map.get((String) selectOneMenuNombre1.getValue())]);
                        //institucionTemp.setNombreDepartamento(normalizarNombreDepartamento(resultado[columnas1Map.get((String) selectOneMenuDepartamento1.getValue())]));
                        institucionTemp.setNombreDepartamento("PARQUE REGIONAL VIRTUAL HOSPITALES ESPECIALIZADOS");
                        if (columnas1Map.get((String) selectOneMenuDistrito1.getValue()) != null) {
                            institucionTemp.setNombreDistrito(normalizarString(normalizarNombreDepartamento(resultado[columnas1Map.get((String) selectOneMenuDepartamento1.getValue())]) 
                                    + " - " + normalizarString(resultado[columnas1Map.get((String) selectOneMenuDistrito1.getValue())])));
                        }else{
                            institucionTemp.setNombreDistrito(normalizarString(normalizarNombreDepartamento(resultado[columnas1Map.get((String) selectOneMenuDepartamento1.getValue())])));
                        }
                        if (columnas1Map.get((String) selectOneMenuCodigo1.getValue()) != null) {
                            institucionTemp.setCodigoPrincipal(normalizarString(normalizarString(resultado[columnas1Map.get((String) selectOneMenuCodigo1.getValue())])));
                        }
                        if (columnas1Map.get((String) selectOneMenuTipo1.getValue()) != null) {
                            institucionTemp.setCodigoSecundario(normalizarString(normalizarString(resultado[columnas1Map.get((String) selectOneMenuTipo1.getValue())])));
                        }
                        institucionTemp.setTipo(1);
                        try {
                            getInstitucionTempFacade().createInstitucionTemp(institucionTemp);
                        }catch (ConstraintViolationException cve) {
                            Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
                            System.out.println("*** Violación de restricciones de la base de datos: " + violations);
                        }catch(Exception e){
                            System.out.println("*** Problema para guardar establecimiento a partir de archivo 1: " + e.getMessage() + " " +
                                    e.getLocalizedMessage());
                        }
                    }

                }
                count++;
            }
            br.close();
            
            // </editor-fold>
            
            System.out.println("******************************Termina insituciones 1: " + new Date());
    
            // <editor-fold defaultstate="collapsed" desc="Lectura del segundo archivo">

            br = new BufferedReader(new FileReader(destinationFileSegundoArchivo));

            System.out.println("******************************Empieza insituciones 2: " + new Date());
            
            while ((line = br.readLine()) != null) {
                
                if (count > 0) { //para ignorar el titulo
                        
                    String[] resultado = line.split(cvsSplitBy);
                    String departamentoFila = normalizarNombreDepartamento(normalizarString(resultado[columnas2Map.get((String) selectOneMenuDepartamento2.getValue())]));
                    
                    //Si el departamento de la fila que se esta leyendo del archivo coincide con el departamento seleccionado 
                    //para machear, se guarda el establecimiento
                    if(departamentoFila.compareTo((String) selectOneMenuDepartamento.getValue())==0){
                        InstitucionTemp institucionTemp = new InstitucionTemp();
                        institucionTemp.setNombreInstitucion(normalizarNombreEntidad(resultado[columnas2Map.get((String) selectOneMenuNombre2.getValue())]));
                        institucionTemp.setNombreInstitucionOriginal(resultado[columnas2Map.get((String) selectOneMenuNombre2.getValue())]);
                        institucionTemp.setNombreDepartamento(normalizarNombreDepartamento(normalizarString(resultado[columnas2Map.get((String) selectOneMenuDepartamento2.getValue())])));
                        if (columnas2Map.get((String) selectOneMenuDistrito2.getValue()) != null) {
                            institucionTemp.setNombreDistrito(normalizarString(normalizarString(resultado[columnas2Map.get((String) selectOneMenuDistrito2.getValue())])));
                        }
                        if (columnas2Map.get((String) selectOneMenuCodigo2.getValue()) != null) {
                            institucionTemp.setCodigoPrincipal(normalizarString(normalizarString(resultado[columnas2Map.get((String) selectOneMenuCodigo2.getValue())])));
                        }
                        if (columnas2Map.get((String) selectOneMenuTipo2.getValue()) != null) {
                            institucionTemp.setCodigoSecundario(resultado[columnas2Map.get((String) selectOneMenuTipo2.getValue())]);
                        }
                        institucionTemp.setTipo(2);
                        try {
                            getInstitucionTempFacade().createInstitucionTemp(institucionTemp);
                        } catch (ConstraintViolationException cve) {
                            Set<ConstraintViolation<?>> violations = cve.getConstraintViolations();
                            System.out.println("*** Violación de restricciones de la base de datos: " + violations);
                        } catch(Exception e){
                            System.out.println("*** Problema para guardar establecimiento a partir de archivo 2: " + e.getMessage() + " " +
                                    e.getLocalizedMessage());
                        }
                    }

                }
                count++;
            }
            br.close();
            
            // </editor-fold>
            
            System.out.println("******************************Termina insituciones 2: " + new Date());
            
        } catch (FileNotFoundException e) {
            System.out.println("No se encuentran los arhivos");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Error al leer los archivos");
            e.printStackTrace();
        } catch (Exception e) {
            keepDialogOpen();
            displayErrorMessageToUser("Ha ocurrido un error", "No se ha podido realizar la actualización " + e.getMessage()
                    + " - " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }
    // </editor-fold>

    public String normalizarNombre(String cadena) {
        //cadena = cadena.replace("HR ",""); //Hospital regional
        cadena = cadena.replace("USF ",""); //Unidad de salud de la familia
        //cadena = cadena.replace("HD ","");  //Hospital distrital
        cadena = cadena.replace("IPS ","");
        return cadena;
    }
    
    public String normalizarNombreDepartamento(String cadena) {
        if (cadena != null) {
            cadena = cadena.trim();
            cadena = cadena.toUpperCase();
            cadena = cadena.replace("Á", "A");
            cadena = cadena.replace("É", "E");
            cadena = cadena.replace("Í", "I");
            cadena = cadena.replace("Ó", "O");
            cadena = cadena.replace("Ú", "U");
            cadena = cadena.replace("XVIII ","");
	    cadena = cadena.replace("VIII ","");
            cadena = cadena.replace("XIII ","");
            cadena = cadena.replace("XVII ","");
	    cadena = cadena.replace("III ","");
            cadena = cadena.replace("VII ","");
	    cadena = cadena.replace("XII ","");
	    cadena = cadena.replace("XVI ","");
            cadena = cadena.replace("XIV ","");
	    cadena = cadena.replace("II ","");
	    cadena = cadena.replace("IV ","");
	    cadena = cadena.replace("VI ","");
	    cadena = cadena.replace("IX ","");
	    cadena = cadena.replace("XI ","");
	    cadena = cadena.replace("XV ","");
	    cadena = cadena.replace("I ","");
            cadena = cadena.replace("V ","");
	    cadena = cadena.replace("X ","");
            cadena = cadena.replace("PTE.","PRESIDENTE");
            cadena = cadena.replace("(", " ");
            cadena = cadena.replace(")", " ");
            cadena = cadena.replace("\"", "");
            //cadena = cadena.replace(" . ", " ");
            cadena = cadena.replace("º", " º ");
            cadena = cadena.replace(" º ", " ");
            cadena = cadena.replace(" -", "-");
            cadena = cadena.replace("- ", "-");
            cadena = cadena.replace(" - ", "-");
            cadena = cadena.replace("    ", " ");
            cadena = cadena.replace("   ", " ");
            cadena = cadena.replace("  ", " ");
            if (cadena.compareTo("") == 0) {
                cadena = null;
            }
        }
        return cadena;
    }
    
    public String normalizarNombreEntidad(String cadena) {
        if (cadena != null) {
            cadena = cadena.trim();
            cadena = cadena.toUpperCase();
            cadena = cadena.replace("Á", "A");
            cadena = cadena.replace("É", "E");
            cadena = cadena.replace("Í", "I");
            cadena = cadena.replace("Ó", "O");
            cadena = cadena.replace("Ú", "U");
            cadena = cadena.replace("-"," "); //reemplazar cada guión por un espacio en blanco
            cadena = cadena.replace(".", ""); //eliminar los puntos
            cadena = cadena.replace("JH", "H"); //ejemplo, jhugua -> hugua
            cadena = cadena.replace(" DE ", " "); //eliminar preposición "de" (stop word)
            cadena = cadena.replace(" DEL ", " "); //eliminar preposición "del" (stop word)
            //cadena = cadena.replace("(", " ");
            //cadena = cadena.replace(")", " ");
            cadena = cadena.replaceAll("\\(.+\\)", "");
            cadena = cadena.replace("\"", "");
            cadena = cadena.replaceAll(" I$", " 1"); 
            cadena = cadena.replaceAll(" II$", " 2");
            //cadena = cadena.replaceAll(" I ", " 1 "); 
            //cadena = cadena.replaceAll(" II ", " 2 ");
            
            cadena = cadena.replaceAll("^KM", "KM "); 
            cadena = cadena.replace(" 7MA ", " 7 MA ");
            cadena = cadena.replace(" 1RA ", " 1 RA ");
            cadena = cadena.replace(" 3RA ", " 3 RA ");
            cadena = cadena.replace(" 5TA ", " 5 TA ");
            cadena = cadena.replace("V", "B");
            //cadena = cadena.replaceAll("(\\d)(\\.)(\\d)", "$1$3"); //Para los nros de escuela con separador de miles, 
            //ejemplo "Esc. Bás. Nº 7.954 Amambay`i"
            //cadena = cadena.replaceAll("(\\d)(\\.)(\\s)", "$1$3"); //Para los nros de escuela que tienen un punto al final 
            // y luego le sigue un espacio en blanco, ejemplo "Esc. Bás. Nº 7954. Amambay`i"
            //cadena = cadena.replaceAll("(\\d)(\\.)([a-zA-Z])", "$1 $3"); //Para los nros de escuela que tienen un punto al final 
            // y luego se omitio el espacio en blanco y sigue una letra direcamente, ejemplo "Esc. Bás. Nº 7954.Amambay`i"
            //cadena = cadena.replace("º", " º ");
            //cadena = cadena.replace(" º ", " ");
            //cadena = cadena.replace(" -", "-");
            cadena = cadena.replace("GRAL.", "GENERAL");
            cadena = cadena.replace("GRAL ", "GENERAL ");
            cadena = cadena.replace("MCAL.", "MARISCAL");
            cadena = cadena.replace("MCAL ", "MARISCAL ");
            cadena = cadena.replace("CPTAN.", "CAPITAN");
            cadena = cadena.replace("CPTAN ", "CAPITAN ");
            cadena = cadena.replace("DR.", "DOCTOR");
            cadena = cadena.replace("DR ", "DOCTOR ");
            cadena = cadena.replace("STA.", "SANTA");
            cadena = cadena.replace("STA ", "SANTA ");
            //if (cadena.compareTo("") == 0) {
            //    cadena = null;
            //}
        }
        String cadena2 = "";
        StringTokenizer tokens = new StringTokenizer(cadena, " ");
        while (tokens.hasMoreTokens()) {
            cadena2 = cadena2 + tokens.nextToken() + " ";
        }
        cadena2 = cadena2.trim();
        return cadena2;
    }

    public String normalizarString(String cadena) {
        if (cadena != null) {
            cadena = cadena.trim();
            cadena = cadena.toUpperCase();
            cadena = cadena.replace("Á", "A");
            cadena = cadena.replace("É", "E");
            cadena = cadena.replace("Í", "I");
            cadena = cadena.replace("Ó", "O");
            cadena = cadena.replace("Ú", "U");
            //cadena = cadena.replace("(", " ");
            //cadena = cadena.replace(")", " ");
            cadena = cadena.replace("\"", "");
            cadena = cadena.replaceAll("\\(.+\\)", "");
            //cadena = cadena.replaceAll("(\\d)(\\.)(\\d)", "$1$3"); //Para los nros de escuela con separador de miles, 
            //ejemplo "Esc. Bás. Nº 7.954 Amambay`i"
            //cadena = cadena.replaceAll("(\\d)(\\.)(\\s)", "$1$3"); //Para los nros de escuela que tienen un punto al final 
            // y luego le sigue un espacio en blanco, ejemplo "Esc. Bás. Nº 7954. Amambay`i"
            //cadena = cadena.replaceAll("(\\d)(\\.)([a-zA-Z])", "$1 $3"); //Para los nros de escuela que tienen un punto al final 
            // y luego se omitio el espacio en blanco y sigue una letra direcamente, ejemplo "Esc. Bás. Nº 7954.Amambay`i"
            cadena = cadena.replace(" . ", " ");
            cadena = cadena.replace("º", " º ");
            cadena = cadena.replace(" º ", " ");
            cadena = cadena.replace(" -", "-");
            cadena = cadena.replace("- ", "-");
            cadena = cadena.replace(" - ", "-");
            cadena = cadena.replace("    ", " ");
            cadena = cadena.replace("   ", " ");
            cadena = cadena.replace("  ", " ");
            cadena = cadena.replace("ESC.", "ESCUELA");
            cadena = cadena.replace("ESC ", "ESCUELA ");
            cadena = cadena.replace("BAS.", "BASICA");
            cadena = cadena.replace("BAS ", "BASICA ");
            cadena = cadena.replace("COL.", "COLEGIO");
            cadena = cadena.replace("COL ", "COLEGIO ");
            cadena = cadena.replace("NAC.", "NACIONAL");
            cadena = cadena.replace("NAC ", "NACIONAL ");
            cadena = cadena.replace("TEC.", "TECNICO");
            cadena = cadena.replace("TEC ", "TECNICO ");
            cadena = cadena.replace("PARROQ.", "PARROQUIAL");
            cadena = cadena.replace("PARROQ ", "PARROQUIAL ");
            cadena = cadena.replace("PRIV.", "PRIVADO");
            cadena = cadena.replace("PRIV ", "PRIVADO ");
            cadena = cadena.replace("SUBV.", "SUBVENCIONADO");
            cadena = cadena.replace("SUBV ", "SUBVENCIONADO ");
            cadena = cadena.replace("TEC ", "TECNICO ");
            cadena = cadena.replace("LIC.", "LICEO");
            cadena = cadena.replace("LIC ", "LICEO ");
            cadena = cadena.replace("I. F. D. ", "INSTITUTO FORMACION DOCENTE");
            cadena = cadena.replace("GRAL.", "GENERAL");
            cadena = cadena.replace("GRAL ", "GENERAL ");
            cadena = cadena.replace("MCAL.", "MARISCAL");
            cadena = cadena.replace("MCAL ", "MARISCAL ");
            cadena = cadena.replace("CPTAN.", "CAPITAN");
            cadena = cadena.replace("CPTAN ", "CAPITAN ");
            cadena = cadena.replace("DR.", "DOCTOR");
            cadena = cadena.replace("DR ", "DOCTOR ");
            cadena = cadena.replace("STA.", "SANTA");
            cadena = cadena.replace("STA ", "SANTA ");
            if (cadena.compareTo("") == 0) {
                cadena = null;
            }
        }
        return cadena;
    }

    float stringMatching(String name1, String name2) {
        int diffLen = name1.length() - name2.length();
        if (diffLen < 0) {
            diffLen = diffLen * (-1);
        }
        if (diffLen < 100) {
        }
        return 0;
    }
    // <editor-fold defaultstate="collapsed" desc="Implementación del data model">
    private LazyDataModel<InstitucionesMacheadas> institucionesMacheadasLazy = null;

    public LazyDataModel<InstitucionesMacheadas> getInstitucionesMacheadas() throws Exception {
        if (institucionesMacheadasLazy == null) {
            institucionesMacheadasLazy = new InstitucionesMacheadasLazyList();
        }
        return institucionesMacheadasLazy;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Implementación del data model">
    private LazyDataModel<InstitucionTemp> institucionesSinMachearLazy = null;

    public LazyDataModel<InstitucionTemp> getInstitucionesSinMachear() throws Exception {
        if (institucionesSinMachearLazy == null) {
            institucionesSinMachearLazy = new InstitucionesSinMachearLazyList();
        }
        return institucionesSinMachearLazy;
    }
    // </editor-fold>
    
    public void execute() {
        System.out.println("*** execute!!!");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Executed", "Using RemoteCommand."));
    }
    
    // <editor-fold defaultstate="collapsed" desc="Implementación del data model">
    private LazyDataModel<InstitucionTemp> institucionesTempLazy = null;

    public LazyDataModel<InstitucionTemp> getInstitucionesTemp() throws Exception {
        if (institucionesTempLazy == null) {
            institucionesTempLazy = new InstitucionTempLazyList();
        }
        return institucionesTempLazy;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Implementación del data model">
    private LazyDataModel<InstitucionesMacheadas> institucionesMacheadasConfirmadasLazy = null;

    public LazyDataModel<InstitucionesMacheadas> getInstitucionesMacheadasConfirmadas() throws Exception {
        if (institucionesMacheadasConfirmadasLazy == null) {
            institucionesMacheadasConfirmadasLazy = new InstitucionesMacheadasConfirmadasLazyList();
        }
        return institucionesMacheadasConfirmadasLazy;
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="Implementación del data model">
    private LazyDataModel<InstitucionesMacheadas> institucionesMacheadasRechazadasLazy = null;

    public LazyDataModel<InstitucionesMacheadas> getInstitucionesMacheadasRechazadas() throws Exception {
        if (institucionesMacheadasRechazadasLazy == null) {
            institucionesMacheadasRechazadasLazy = new InstitucionesMacheadasRechazadasLazyList();
        }
        return institucionesMacheadasRechazadasLazy;
    }
    // </editor-fold>

    InstitucionesMacheadas filaSeleccionada;

    public InstitucionesMacheadas getFilaSeleccionada() {
        return filaSeleccionada;
    }

    public void setFilaSeleccionada(InstitucionesMacheadas filaSeleccionada) {
        this.filaSeleccionada = filaSeleccionada;
    }
    
    InstitucionesMacheadas filaConfirmadaSeleccionada;

    public InstitucionesMacheadas getFilaConfirmadaSeleccionada() {
        return filaConfirmadaSeleccionada;
    }

    public void setFilaConfirmadaSeleccionada(InstitucionesMacheadas filaConfirmadaSeleccionada) {
        this.filaConfirmadaSeleccionada = filaConfirmadaSeleccionada;
    }
    
    InstitucionesMacheadas filaRechazadaSeleccionada;

    public InstitucionesMacheadas getFilaRechazadaSeleccionada() {
        return filaRechazadaSeleccionada;
    }

    public void setFilaRechazadaSeleccionada(InstitucionesMacheadas filaRechazadaSeleccionada) {
        this.filaRechazadaSeleccionada = filaRechazadaSeleccionada;
    }
    
    InstitucionTemp sinMachearSeleccionado;

    public InstitucionTemp getSinMachearSeleccionado() {
        return sinMachearSeleccionado;
    }

    public void setSinMachearSeleccionado(InstitucionTemp sinMachearSeleccionado) {
        this.sinMachearSeleccionado = sinMachearSeleccionado;
    }
    
    InstitucionTemp opcionMachearSeleccionado;

    public InstitucionTemp getOpcionMachearSeleccionado() {
        return opcionMachearSeleccionado;
    }

    public void setOpcionMachearSeleccionado(InstitucionTemp opcionMachearSeleccionado) {
        this.opcionMachearSeleccionado = opcionMachearSeleccionado;
    }

    // <editor-fold defaultstate="collapsed" desc="Método onclick para delete">
    public void confirmarRechazar(InstitucionesMacheadas im) {
        String estado = (String) selectOneMenuEstado.getValue();
        filaSeleccionada = im;
        if(estado.compareTo("CONFIRMAR")==0){
            RequestContext rcontext = RequestContext.getCurrentInstance();
            rcontext.execute("institucionMacheadaConfirmarDialogWidget.show()");
        }else if(estado.compareTo("RECHAZAR")==0){
            RequestContext rcontext = RequestContext.getCurrentInstance();
            rcontext.execute("institucionMacheadaDeleteDialogWidget.show()");
        }
    }
    // </editor-fold>

    public void rechazarInstitucionMacheada() {
        try {
            filaSeleccionada.setEstado("RECHAZADO");
            getInstitucionesMacheadasFacade().updateInstitucionesMacheadas(filaSeleccionada);
            tabIndex = 0;
            displayInfoMessageToUser("Rechazado con éxito", "La fila seleccionada se ha rechazado exitosamente");
            printObjectivesToFile("LOG_OPERACIONES.txt", "RECHAZAR;" + filaSeleccionada.getInstitucion1().getCodigoPrincipal() + ";" + filaSeleccionada.getInstitucion2().getCodigoPrincipal()+";");
        } catch (Exception e) {
            displayErrorMessageToUser("Ha ocurrido un error", "No se ha podido rechazar la fila seleccionada " + e.getMessage());
        }
    }
    
    public void eliminarFilaConfirmada() {
        try {
            if(filaConfirmadaSeleccionada.getModo().compareTo("AUTOMATICO")==0){
                filaConfirmadaSeleccionada.setEstado("SIN ESTADO");
                getInstitucionesMacheadasFacade().updateInstitucionesMacheadas(filaConfirmadaSeleccionada);
                printObjectivesToFile("LOG_OPERACIONES.txt", "DESCONFIRMAR AUTOMATICO;" + filaConfirmadaSeleccionada.getInstitucion1().getCodigoPrincipal() + ";" + filaConfirmadaSeleccionada.getInstitucion2().getCodigoPrincipal()+";");
            }
            if(filaConfirmadaSeleccionada.getModo().compareTo("USUARIO")==0){
                getInstitucionesMacheadasFacade().deleteInstitucionesMacheadas(filaConfirmadaSeleccionada);
                printObjectivesToFile("LOG_OPERACIONES.txt", "DESCONFIRMAR MANUAL;" + filaConfirmadaSeleccionada.getInstitucion1().getCodigoPrincipal() + ";" + filaConfirmadaSeleccionada.getInstitucion2().getCodigoPrincipal()+";");
            }
            tabIndex = 1;
            displayInfoMessageToUser("Desconfirmado con éxito", "La fila seleccionada se ha desconfirmado exitosamente");
        } catch (Exception e) {
            displayErrorMessageToUser("Ha ocurrido un error", "No se ha podido desconfirmar la fila seleccionada " + e.getMessage());
        }
    }
    
    public void eliminarFilaRechazada() {
        try {
            filaRechazadaSeleccionada.setEstado("SIN ESTADO");
            getInstitucionesMacheadasFacade().updateInstitucionesMacheadas(filaRechazadaSeleccionada);
            tabIndex = 2;
            displayInfoMessageToUser("Des-rechazado con éxito", "La fila seleccionada se ha aceptado de nuevo exitosamente");
            printObjectivesToFile("LOG_OPERACIONES.txt", "DES-RECHAZAR;" + filaRechazadaSeleccionada.getInstitucion1().getCodigoPrincipal() + ";" + filaRechazadaSeleccionada.getInstitucion2().getCodigoPrincipal()+";");
        } catch (Exception e) {
            displayErrorMessageToUser("Ha ocurrido un error", "No se ha podido aceptar de nuevo la fila seleccionada " + e.getMessage());
        }
    }
    
    public void confirmarInstitucionMacheada() {
        try {
            filaSeleccionada.setEstado("CONFIRMADO");
            getInstitucionesMacheadasFacade().updateInstitucionesMacheadas(filaSeleccionada);
            tabIndex = 0;
            displayInfoMessageToUser("Confirmado con éxito", "La fila seleccionada se ha confirmado exitosamente");
            printObjectivesToFile("LOG_OPERACIONES.txt", "CONFIRMAR;" + filaSeleccionada.getInstitucion1().getCodigoPrincipal() + ";" + filaSeleccionada.getInstitucion2().getCodigoPrincipal()+";");
        } catch (Exception e) {
            displayErrorMessageToUser("Ha ocurrido un error", "No se ha podido confirmar la fila seleccionada " + e.getMessage());
        }
    }
}
